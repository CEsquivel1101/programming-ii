package Programming2.LabW7.Activity1;

import Programming2.LabW7.Activity1.Pieces.Piece;

import java.util.Random;

public class Board {
    private Piece[][] squares;

    public Board() {
        squares = new Piece[8][8];
        initializeBoard();
    }

    private void initializeBoard() {
        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                squares[row][col] = null;
            }
        }
    }

    public void placePieceRandomly(Piece piece) {
        Random rand = new Random();
        int row, col;
        do {
            row = rand.nextInt(8);
            col = rand.nextInt(8);
        } while (squares[row][col] != null);
        squares[row][col] = piece;
    }

    public void placePiece(Piece piece) {
        squares[piece.getInitialRow()][piece.getInitialCol()] = piece;
    }

    public void displayBoard() {
        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                System.out.print(squares[row][col] == null ? ". " : squares[row][col].getColoredSymbol() + " ");
            }
            System.out.println();
        }
        System.out.println();
    }

    public Piece[][] getSquares() {
        return squares;
    }

    public void setSquares(Piece[][] squares) {
        this.squares = squares;
    }
}