package Programming2.LabW7.Activity1.Sorting;

import Programming2.LabW7.Activity1.Board;
import Programming2.LabW7.Activity1.Pieces.Piece;

import java.util.Arrays;

public class RadixSort implements SortingAlgorithm {
    @Override
    public void sort(Piece[] pieces, String sortBy, Board board, int delay) throws InterruptedException {
        if (sortBy.equalsIgnoreCase("n")) {
            radixSortByNumber(pieces, board, delay);
        } else {
            radixSortByCharacter(pieces, board, delay);
        }
    }

    private void radixSortByNumber(Piece[] pieces, Board board, int delay) throws InterruptedException {
        int maxNumber = getMaxNumber(pieces);
        for (int exp = 1; maxNumber / exp > 0; exp *= 10) {
            countingSortByNumber(pieces, exp, board, delay);
        }
    }

    private int getMaxNumber(Piece[] pieces) {
        int max = pieces[0].getNumber();
        for (Piece piece : pieces) {
            if (piece.getNumber() > max) {
                max = piece.getNumber();
            }
        }
        return max;
    }

    private void countingSortByNumber(Piece[] pieces, int exp, Board board, int delay) throws InterruptedException {
        Piece[] output = new Piece[pieces.length];
        int[] count = new int[10];
        Arrays.fill(count, 0);

        for (Piece piece : pieces) {
            count[(piece.getNumber() / exp) % 10]++;
        }

        for (int i = 1; i < 10; i++) {
            count[i] += count[i - 1];
        }

        for (int i = pieces.length - 1; i >= 0; i--) {
            output[count[(pieces[i].getNumber() / exp) % 10] - 1] = pieces[i];
            count[(pieces[i].getNumber() / exp) % 10]--;
        }

        for (int i = 0; i < pieces.length; i++) {
            pieces[i] = output[i];
            updateBoard(board, pieces, delay);
        }
    }

    private void radixSortByCharacter(Piece[] pieces, Board board, int delay) throws InterruptedException {
        int maxChar = getMaxChar(pieces);
        for (int exp = 1; maxChar / exp > 0; exp *= 10) {
            countingSortByCharacter(pieces, exp, board, delay);
        }
    }

    private int getMaxChar(Piece[] pieces) {
        char max = pieces[0].getCharacter();
        for (Piece piece : pieces) {
            if (piece.getCharacter() > max) {
                max = piece.getCharacter();
            }
        }
        return max;
    }

    private void countingSortByCharacter(Piece[] pieces, int exp, Board board, int delay) throws InterruptedException {
        Piece[] output = new Piece[pieces.length];
        int[] count = new int[256];
        Arrays.fill(count, 0);

        for (Piece piece : pieces) {
            count[(piece.getCharacter() / exp) % 256]++;
        }

        for (int i = 1; i < 256; i++) {
            count[i] += count[i - 1];
        }

        for (int i = pieces.length - 1; i >= 0; i--) {
            output[count[(pieces[i].getCharacter() / exp) % 256] - 1] = pieces[i];
            count[(pieces[i].getCharacter() / exp) % 256]--;
        }

        for (int i = 0; i < pieces.length; i++) {
            pieces[i] = output[i];
            updateBoard(board, pieces, delay);
        }
    }

    private void updateBoard(Board board, Piece[] pieces, int delay) throws InterruptedException {
        board.setSquares(convertToBoard(pieces));
        board.displayBoard();
        Thread.sleep(delay);
    }

    private Piece[][] convertToBoard(Piece[] pieces) {
        Piece[][] board = new Piece[8][8];
        for (Piece piece : pieces) {
            if (piece != null) {
                board[piece.getInitialRow()][piece.getInitialCol()] = piece;
            }
        }
        return board;
    }
}