package Programming2.LabW7.Activity1.Sorting;

import Programming2.LabW7.Activity1.Board;
import Programming2.LabW7.Activity1.Pieces.Piece;

public class InsertionSort implements SortingAlgorithm {
    @Override
    public void sort(Piece[] pieces, String sortBy, Board board, int delay) throws InterruptedException {
        for (int i = 1; i < pieces.length; i++) {
            Piece key = pieces[i];
            int j = i - 1;
            while (j >= 0 && compare(pieces[j], key, sortBy) > 0) {
                pieces[j + 1] = pieces[j];
                j--;
                updateBoard(board, pieces, delay);
            }
            pieces[j + 1] = key;
            updateBoard(board, pieces, delay);
        }
    }

    private int compare(Piece p1, Piece p2, String sortBy) {
        return sortBy.equalsIgnoreCase("n") ?
                Integer.compare(p1.getNumber(), p2.getNumber()) :
                Character.compare(p1.getCharacter(), p2.getCharacter());
    }

    private void updateBoard(Board board, Piece[] pieces, int delay) throws InterruptedException {
        board.setSquares(convertToBoard(pieces));
        board.displayBoard();
        Thread.sleep(delay);
    }

    private Piece[][] convertToBoard(Piece[] pieces) {
        Piece[][] board = new Piece[8][8];
        for (Piece piece : pieces) {
            if (piece != null) {
                board[piece.getInitialRow()][piece.getInitialCol()] = piece;
            }
        }
        return board;
    }
}