package Programming2.LabW7.Activity1.Sorting;

import Programming2.LabW7.Activity1.Board;
import Programming2.LabW7.Activity1.Pieces.Piece;

public interface SortingAlgorithm {
    void sort(Piece[] pieces, String sortBy, Board board, int delay) throws InterruptedException;
}