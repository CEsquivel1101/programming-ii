package Programming2.LabW7.Activity1.Sorting;

import Programming2.LabW7.Activity1.Board;
import Programming2.LabW7.Activity1.Pieces.Piece;

import java.util.Arrays;

public class MergeSort implements SortingAlgorithm {
    @Override
    public void sort(Piece[] pieces, String sortBy, Board board, int delay) throws InterruptedException {
        if (pieces.length > 1) {
            int mid = pieces.length / 2;
            Piece[] left = Arrays.copyOfRange(pieces, 0, mid);
            Piece[] right = Arrays.copyOfRange(pieces, mid, pieces.length);
            sort(left, sortBy, board, delay);
            sort(right, sortBy, board, delay);
            merge(pieces, left, right, sortBy, board, delay);
        }
    }

    private void merge(Piece[] pieces, Piece[] left, Piece[] right, String sortBy, Board board, int delay) throws InterruptedException {
        int i = 0, j = 0, k = 0;
        while (i < left.length && j < right.length) {
            pieces[k++] = compare(left[i], right[j], sortBy) <= 0 ? left[i++] : right[j++];
            updateBoard(board, pieces, delay);
        }
        while (i < left.length) {
            pieces[k++] = left[i++];
            updateBoard(board, pieces, delay);
        }
        while (j < right.length) {
            pieces[k++] = right[j++];
            updateBoard(board, pieces, delay);
        }
    }

    private int compare(Piece p1, Piece p2, String sortBy) {
        return sortBy.equalsIgnoreCase("n") ?
                Integer.compare(p1.getNumber(), p2.getNumber()) :
                Character.compare(p1.getCharacter(), p2.getCharacter());
    }

    private void updateBoard(Board board, Piece[] pieces, int delay) throws InterruptedException {
        board.setSquares(convertToBoard(pieces));
        board.displayBoard();
        Thread.sleep(delay);
    }

    private Piece[][] convertToBoard(Piece[] pieces) {
        Piece[][] board = new Piece[8][8];
        for (Piece piece : pieces) {
            if (piece != null) {
                board[piece.getInitialRow()][piece.getInitialCol()] = piece;
            }
        }
        return board;
    }
}