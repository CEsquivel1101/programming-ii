package Programming2.LabW7.Activity1.Pieces;

public class Bishop extends Piece {
    public Bishop(int number, char character, int initialCol, String color) {
        super('B', "Bishop " + number, number, character, 0, initialCol, color);
    }
}