package Programming2.LabW7.Activity1.Pieces;

public class Pawn extends Piece {
    public Pawn(int number, char character, int initialCol, String color) {
        super('P', "Pawn " + number, number, character, 1, initialCol, color);
    }
}