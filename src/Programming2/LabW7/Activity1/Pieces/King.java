package Programming2.LabW7.Activity1.Pieces;

public class King extends Piece {
    public King(int number, char character, String color) {
        super('K', "King", number, character, 0, 4, color);
    }
}