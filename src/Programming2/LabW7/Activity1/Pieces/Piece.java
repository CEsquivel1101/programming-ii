package Programming2.LabW7.Activity1.Pieces;

public abstract class Piece {
    private final char symbol;
    private final String name;
    private final int number;
    private final char character;
    private final int initialRow;
    private final int initialCol;
    private final String color;

    public Piece(char symbol, String name, int number, char character, int initialRow, int initialCol, String color) {
        this.symbol = symbol;
        this.name = name;
        this.number = number;
        this.character = character;
        this.initialRow = initialRow;
        this.initialCol = initialCol;
        this.color = color;
    }

    public char getSymbol() {
        return symbol;
    }

    public String getName() {
        return name;
    }

    public int getNumber() {
        return number;
    }

    public char getCharacter() {
        return character;
    }

    public int getInitialRow() {
        return initialRow;
    }

    public int getInitialCol() {
        return initialCol;
    }

    public String getColor() {
        return color;
    }

    public String getColoredSymbol() {
        return color.equalsIgnoreCase("b") ? "\u001B[31m" + symbol + "\u001B[0m" : "\u001B[37m" + symbol + "\u001B[0m";
    }
}