package Programming2.LabW7.Activity1.Pieces;

public class Knight extends Piece {
    public Knight(int number, char character, int initialCol, String color) {
        super('N', "Knight " + number, number, character, 0, initialCol, color);
    }
}
