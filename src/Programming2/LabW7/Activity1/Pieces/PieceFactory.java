package Programming2.LabW7.Activity1.Pieces;

import java.util.ArrayList;
import java.util.List;

public class PieceFactory {
    private static final char[] characters = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p'};
    private static final int[] numbers = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16};

    public static List<Piece> createPieces(int numPieces, String color) {
        List<Piece> pieces = new ArrayList<>();
        switch (numPieces) {
            case 1:
                pieces.add(new King(numbers[0], characters[0], color));
                break;
            case 2:
                pieces.add(new King(numbers[0], characters[0], color));
                pieces.add(new Queen(numbers[1], characters[1], color));
                break;
            case 4:
                pieces.add(new King(numbers[0], characters[0], color));
                pieces.add(new Queen(numbers[1], characters[1], color));
                pieces.add(new Bishop(numbers[4], characters[4], 2, color));
                pieces.add(new Bishop(numbers[5], characters[5], 5, color));
                break;
            case 6:
                pieces.add(new King(numbers[0], characters[0], color));
                pieces.add(new Queen(numbers[1], characters[1], color));
                pieces.add(new Bishop(numbers[4], characters[4], 2, color));
                pieces.add(new Bishop(numbers[5], characters[5], 5, color));
                pieces.add(new Knight(numbers[6], characters[6], 1, color));
                pieces.add(new Knight(numbers[7], characters[7], 6, color));
                break;
            case 8:
                pieces.add(new King(numbers[0], characters[0], color));
                pieces.add(new Queen(numbers[1], characters[1], color));
                pieces.add(new Bishop(numbers[4], characters[4], 2, color));
                pieces.add(new Bishop(numbers[5], characters[5], 5, color));
                pieces.add(new Knight(numbers[6], characters[6], 1, color));
                pieces.add(new Knight(numbers[7], characters[7], 6, color));
                pieces.add(new Rook(numbers[2], characters[2], 0, color));
                pieces.add(new Rook(numbers[3], characters[3], 7, color));
                break;
            case 10:
                pieces.add(new King(numbers[0], characters[0], color));
                pieces.add(new Queen(numbers[1], characters[1], color));
                pieces.add(new Bishop(numbers[4], characters[4], 2, color));
                pieces.add(new Bishop(numbers[5], characters[5], 5, color));
                pieces.add(new Knight(numbers[6], characters[6], 1, color));
                pieces.add(new Knight(numbers[7], characters[7], 6, color));
                pieces.add(new Rook(numbers[2], characters[2], 0, color));
                pieces.add(new Rook(numbers[3], characters[3], 7, color));
                pieces.add(new Pawn(numbers[8], characters[8], 0, color));
                pieces.add(new Pawn(numbers[9], characters[9], 1, color));
                break;
            case 16:
                pieces.add(new King(numbers[0], characters[0], color));
                pieces.add(new Queen(numbers[1], characters[1], color));
                pieces.add(new Rook(numbers[2], characters[2], 0, color));
                pieces.add(new Rook(numbers[3], characters[3], 7, color));
                pieces.add(new Bishop(numbers[4], characters[4], 2, color));
                pieces.add(new Bishop(numbers[5], characters[5], 5, color));
                pieces.add(new Knight(numbers[6], characters[6], 1, color));
                pieces.add(new Knight(numbers[7], characters[7], 6, color));
                for (int i = 8; i <= 15; i++) {
                    pieces.add(new Pawn(numbers[i], characters[i], i - 8, color));
                }
                break;
            default:
                throw new IllegalArgumentException("Invalid number of pieces: " + numPieces);
        }
        return pieces;
    }
}