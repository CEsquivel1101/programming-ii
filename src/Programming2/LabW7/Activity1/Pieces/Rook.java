package Programming2.LabW7.Activity1.Pieces;

public class Rook extends Piece {
    public Rook(int number, char character, int initialCol, String color) {
        super('R', "Rook " + number, number, character, 0, initialCol, color);
    }
}