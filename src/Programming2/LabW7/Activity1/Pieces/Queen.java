package Programming2.LabW7.Activity1.Pieces;

public class Queen extends Piece {
    public Queen(int number, char character, String color) {
        super('Q', "Queen", number, character, 0, 3, color);
    }
}