package Programming2.LabW7.Activity1;

import Programming2.LabW7.Activity1.Pieces.*;
import Programming2.LabW7.Activity1.Sorting.*;

import java.util.*;

public class Chess {
    private final Board board;
    private final List<Piece> pieces;
    private SortingAlgorithm sortingAlgorithm;
    private final int delay;

    public Chess(String sortingMethod, String pieceType, int numPieces, int delay, String color) {
        this.board = new Board();
        this.delay = delay;
        this.pieces = PieceFactory.createPieces(numPieces, color);
        placePiecesRandomlyOnBoard();
        board.displayBoard();
        selectSortingAlgorithm(sortingMethod);
        sortAndPlacePieces(pieceType);
        board.displayBoard();
    }

    private void placePiecesRandomlyOnBoard() {
        for (Piece piece : pieces) {
            board.placePieceRandomly(piece);
        }
    }

    private void selectSortingAlgorithm(String sortingMethod) {
        switch (sortingMethod.toLowerCase()) {
            case "i":
                sortingAlgorithm = new InsertionSort();
                break;
            case "m":
                sortingAlgorithm = new MergeSort();
                break;
            case "r":
                sortingAlgorithm = new RadixSort();
                break;
            default:
                throw new IllegalArgumentException("Invalid sorting method: " + sortingMethod);
        }
    }

    private void sortAndPlacePieces(String sortBy) {
        if (!sortBy.equals("n") && !sortBy.equals("c")) {
            throw new IllegalArgumentException("Invalid piece type for sorting: " + sortBy);
        }

        List<Piece> piecesOnBoard = extractPiecesFromBoard();
        Piece[] piecesArray = piecesOnBoard.toArray(new Piece[0]);

        try {
            sortingAlgorithm.sort(piecesArray, sortBy, board, delay);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

        pieces.clear();
        Collections.addAll(pieces, piecesArray);
    }

    private List<Piece> extractPiecesFromBoard() {
        List<Piece> piecesOnBoard = new ArrayList<>();
        Piece[][] squares = board.getSquares();
        for (int row = 0; row < 8; row++) {
            for (int col = 0; col < 8; col++) {
                if (squares[row][col] != null) {
                    piecesOnBoard.add(squares[row][col]);
                    squares[row][col] = null;
                }
            }
        }
        return piecesOnBoard;
    }

    public static void main(String[] args) {
        try {
            String sortingMethod = "";
            String pieceType = "";
            int numPieces = 0;
            int delay = 0;
            String color = "";

            for (String arg : args) {
                if (arg.startsWith("a=")) {
                    sortingMethod = arg.substring(2);
                } else if (arg.startsWith("t=")) {
                    pieceType = arg.substring(2);
                } else if (arg.startsWith("c=")) {
                    color = arg.substring(2);
                } else if (arg.startsWith("r=")) {
                    numPieces = Integer.parseInt(arg.substring(2));
                } else if (arg.startsWith("s=")) {
                    delay = Integer.parseInt(arg.substring(2));
                }
            }

            validateParameters(sortingMethod, pieceType, color, numPieces, delay);

            new Chess(sortingMethod, pieceType, numPieces, delay, color);
        } catch (IllegalArgumentException e) {
            System.err.println("Error: " + e.getMessage());
            System.err.println("Usage: java Chess a=<sortingMethod> t=<pieceType> c=<color> r=<number> s=<delay>");
        }
    }

    private static void validateParameters(String sortingMethod, String pieceType, String color, int numPieces, int delay) {
        Set<String> validSortingMethods = new HashSet<>(Arrays.asList("i", "m", "r"));
        Set<String> validPieceTypes = new HashSet<>(Arrays.asList("n", "c"));
        Set<String> validColors = new HashSet<>(Arrays.asList("w", "b"));

        if (!validSortingMethods.contains(sortingMethod.toLowerCase())) {
            throw new IllegalArgumentException("Invalid sorting method: " + sortingMethod);
        }

        if (!validPieceTypes.contains(pieceType.toLowerCase())) {
            throw new IllegalArgumentException("Invalid piece type: " + pieceType);
        }

        if (!validColors.contains(color.toLowerCase())) {
            throw new IllegalArgumentException("Invalid color: " + color);
        }

        if (numPieces <= 0 || numPieces > 16) {
            throw new IllegalArgumentException("Invalid number of pieces: " + numPieces + ". Must be between 1 and 16.");
        }

        if (delay < 0) {
            throw new IllegalArgumentException("Invalid delay: " + delay + ". Must be a non-negative integer.");
        }
    }
}
