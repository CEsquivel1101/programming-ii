package Programming2.LabW7.Activity2;

abstract class Shape {
    public abstract double calculateArea();
    public abstract double calculatePerimeter();
}
