package Programming2.LabW7.Activity2;

import java.util.ArrayList;
import java.util.List;

public class Main {
    public static void main(String[] args) {
        System.out.println("** Calculate area and perimeter of shapes **");

        List<Shape> shapes = new ArrayList<>();
        shapes.add(new Rectangle(2.0, 8.0));
        shapes.add(new Circle(5.0));
        shapes.add(new Triangle(7.0, 4.0));
        shapes.add(new Square(4.0));

        for (Shape shape : shapes) {
            if (shape instanceof Rectangle) {
                Rectangle rect = (Rectangle) shape;
                System.out.printf("Rectangle:  Width = %.1f Height = %.1f\n", rect.getWidth(), rect.getHeight());
            } else if (shape instanceof Circle) {
                Circle circle = (Circle) shape;
                System.out.printf("Circle:  Radius = %.1f\n", circle.getRadius());
            } else if (shape instanceof Triangle) {
                Triangle triangle = (Triangle) shape;
                System.out.printf("Triangle:  Height = %.1f Base = %.1f\n", triangle.getHeight(), triangle.getBase());
            } else if (shape instanceof Square) {
                Square square = (Square) shape;
                System.out.printf("Square:  Side = %.1f\n", square.getSide());
            }

            System.out.println("Area " + shape.calculateArea());
            System.out.println("Perimeter " + shape.calculatePerimeter());
            System.out.println();
        }
    }
}