package Programming2.LabW6.Activity1;
import java.util.LinkedList;

class PriorityQueueTask extends TaskQueue {
    private int capacity;
    private AuxiliaryQueue auxiliaryQueue;

    public PriorityQueueTask(int capacity, AuxiliaryQueue auxiliaryQueue) {
        this.capacity = capacity;
        this.auxiliaryQueue = auxiliaryQueue;
        this.tasks = new LinkedList<>();
    }

    @Override
    public void addTask(Task task) {
        if (tasks.size() < capacity) {
            insertInPriorityOrder(task);
        } else {
            auxiliaryQueue.addTask(task);
        }
    }

    public void processNextTask() {
        if (!tasks.isEmpty()) {
            tasks.poll();
            if (!auxiliaryQueue.isEmpty()) {
                Task nextAuxiliaryTask = auxiliaryQueue.getNextTask();
                insertInPriorityOrder(nextAuxiliaryTask);
            }
        }
    }

    private void insertInPriorityOrder(Task task) {
        if (tasks.isEmpty() || task.getPriority() >= tasks.peek().getPriority()) {
            ((LinkedList<Task>) tasks).addFirst(task);
        } else {
            boolean inserted = false;
            for (int i = 0; i < ((LinkedList<Task>) tasks).size(); i++) {
                if (task.getPriority() >= ((LinkedList<Task>) tasks).get(i).getPriority()) {
                    ((LinkedList<Task>) tasks).add(i, task);
                    inserted = true;
                    break;
                }
            }
            if (!inserted) {
                tasks.add(task);
            }
        }
    }

    @Override
    public void displayTasks() {
        System.out.println("Priority Queue");
        for (Task task : tasks) {
            System.out.println(task);
        }
    }
}
