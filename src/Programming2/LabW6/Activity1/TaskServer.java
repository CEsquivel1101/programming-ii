package Programming2.LabW6.Activity1;
class TaskServer {
    private PriorityQueueTask priorityQueueTask;
    private AuxiliaryQueue auxiliaryQueue;

    public TaskServer(int priorityQueueCapacity) {
        this.auxiliaryQueue = new AuxiliaryQueue();
        this.priorityQueueTask = new PriorityQueueTask(priorityQueueCapacity, auxiliaryQueue);
    }

    public void initializeTasks(Task[] tasks) {
        for (Task task : tasks) {
            priorityQueueTask.addTask(task);
        }
        priorityQueueTask.displayTasks();
        auxiliaryQueue.displayTasks();
        System.out.println("====================================");
    }

    public void processAllTasks() {
        while (!priorityQueueTask.tasks.isEmpty() || !auxiliaryQueue.isEmpty()) {
            priorityQueueTask.processNextTask();
            priorityQueueTask.displayTasks();
            auxiliaryQueue.displayTasks();
            System.out.println("====================================");
        }
        System.out.println("No more tasks to perform.");
    }

    public static void main(String[] args) {
        TaskServer server = new TaskServer(5);

        Task[] tasks = {
                new Task("Task 1", 1),
                new Task("Task 2", 2),
                new Task("Task 3", 1),
                new Task("Task 4", 3),
                new Task("Task 5", 1),
                new Task("Task 6", 2),
                new Task("Task 7", 3),
                new Task("Task 8", 1),
                new Task("Task 9", 2),
                new Task("Task 10", 3),
                new Task("Task 11", 10),
                new Task("Task 12", 1),
                new Task("Task 13", 3),
                new Task("Task 14", 2),
                new Task("Task 15", 3),
        };

        server.initializeTasks(tasks);

        server.processAllTasks();
    }
}
