package Programming2.LabW6.Activity1;

import java.util.Queue;

abstract class TaskQueue {
    protected Queue<Task> tasks;

    public abstract void addTask(Task task);
    public abstract void displayTasks();
}
