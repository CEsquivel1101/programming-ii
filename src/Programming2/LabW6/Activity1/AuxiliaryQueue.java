package Programming2.LabW6.Activity1;

import java.util.LinkedList;

class AuxiliaryQueue extends TaskQueue {
    public AuxiliaryQueue() {
        this.tasks = new LinkedList<>();
    }

    @Override
    public void addTask(Task task) {
        tasks.add(task);
    }

    public boolean isEmpty() {
        return tasks.isEmpty();
    }

    public Task getNextTask() {
        return ((LinkedList<Task>) tasks).poll();
    }

    @Override
    public void displayTasks() {
        System.out.println("Auxiliary Queue");
        for (Task task : tasks) {
            System.out.println(task);
        }
    }
}
