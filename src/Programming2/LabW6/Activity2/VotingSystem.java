package Programming2.LabW6.Activity2;

interface VotingSystem {
    void castVote(Vote vote);
    void displayResults();
}
