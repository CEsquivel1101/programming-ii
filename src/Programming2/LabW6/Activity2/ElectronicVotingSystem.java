package Programming2.LabW6.Activity2;

import java.util.HashMap;
import java.util.Map;

class ElectronicVotingSystem implements VotingSystem {
    private Map<String, Integer> voteCount;

    public ElectronicVotingSystem() {
        this.voteCount = new HashMap<>();
    }

    @Override
    public void castVote(Vote vote) {
        if (vote.isValid()) {
            Candidate candidate = vote.getSelectedCandidate();
            String candidateName = candidate.getName();

            voteCount.put(candidateName, voteCount.getOrDefault(candidateName, 0) + 1);
        } else {
            System.out.println("Invalid vote. The vote is discarded.");
        }
    }

    @Override
    public void displayResults() {
        System.out.println("Election Results:");

        voteCount.entrySet().stream()
                .sorted(Map.Entry.<String, Integer>comparingByValue().reversed())
                .forEach(entry -> System.out.println(entry.getKey() + " : " + entry.getValue()));
    }
}
