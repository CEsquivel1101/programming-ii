package Programming2.LabW6.Activity2;

import java.util.List;

public class Main {
    public static void main(String[] args) {
        VotingSystem votingSystem = new ElectronicVotingSystem();

        Candidate candidateA = new Candidate("Candidate A");
        Candidate candidateB = new Candidate("Candidate B");
        Candidate candidateC = new Candidate("Candidate C");

        Vote[] votes = {
                new Vote(List.of(candidateA)),
                new Vote(List.of(candidateA)),
                new Vote(List.of(candidateA, candidateB)),// Invalid vote for having more than one candidate selected
                new Vote(List.of(candidateC, candidateB)),// Invalid vote for having more than one candidate selected
                new Vote(List.of(candidateB)),
                new Vote(List.of(candidateC)),
                new Vote(List.of(candidateA)),
                new Vote(List.of(candidateC)),
                new Vote(List.of(candidateB)),
                new Vote(List.of(candidateB)),
                new Vote(List.of(candidateA)),
                new Vote(List.of(candidateA)),
        };

        for (Vote vote : votes) {
            votingSystem.castVote(vote);
        }

        votingSystem.displayResults();
    }
}
