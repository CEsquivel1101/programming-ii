package Programming2.LabW6.Activity2;

import java.util.List;

class Vote {
    private List<Candidate> selectedCandidates;

    public Vote(List<Candidate> selectedCandidates) {
        this.selectedCandidates = selectedCandidates;
    }

    public boolean isValid() {
        return selectedCandidates.size() == 1;
    }

    public Candidate getSelectedCandidate() {
        return isValid() ? selectedCandidates.get(0) : null;
    }
}
