package Programming2.LabW4.Activity1;
import java.util.Comparator;

public interface Sortable<T> {
    void sort();

    void sortBy(Comparator<T> comparator);
}

