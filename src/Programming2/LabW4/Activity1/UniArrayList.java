package Programming2.LabW4.Activity1;
import java.util.Comparator;
import java.util.Iterator;
import java.util.NoSuchElementException;

public class UniArrayList<T> implements java.util.List<T>, Sortable<T>, Unique<T> {
    private Object[] array;
    private int size;

    public UniArrayList() {
        array = new Object[10];
        size = 0;
    }

    public UniArrayList(T[] initialArray) {
        array = new Object[initialArray.length];
        size = initialArray.length;
        for (int i = 0; i < size; i++) {
            array[i] = initialArray[i];
        }
    }

    private void ensureCapacity() {
        if (size == array.length) {
            Object[] newArray = new Object[size * 2];
            for (int i = 0; i < size; i++) {
                newArray[i] = array[i];
            }
            array = newArray;
        }
    }

    @Override
    public boolean add(T element) {
        ensureCapacity();
        array[size++] = element;
        return true;
    }

    @Override
    public void add(int index, T element) {
        if (index < 0 || index > size) throw new IndexOutOfBoundsException();
        ensureCapacity();
        for (int i = size; i > index; i--) {
            array[i] = array[i - 1];
        }
        array[index] = element;
        size++;
    }

    @Override
    public T remove(int index) {
        if (index < 0 || index >= size) throw new IndexOutOfBoundsException();
        T removedElement = (T) array[index];
        for (int i = index; i < size - 1; i++) {
            array[i] = array[i + 1];
        }
        array[--size] = null;
        return removedElement;
    }

    @Override
    public boolean remove(Object obj) {
        for (int index = 0; index < size; index++) {
            if (array[index].equals(obj)) {
                remove(index);
                return true;
            }
        }
        return false;
    }

    @Override
    public T get(int index) {
        if (index < 0 || index >= size) throw new IndexOutOfBoundsException();
        return (T) array[index];
    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void sort() {
        for (int i = 0; i < size - 1; i++) {
            for (int j = 0; j < size - 1 - i; j++) {
                if (((Comparable<T>) array[j]).compareTo((T) array[j + 1]) > 0) {
                    T temp = (T) array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }

    @Override
    public void sortBy(Comparator<T> comparator) {
        for (int i = 0; i < size - 1; i++) {
            for (int j = 0; j < size - 1 - i; j++) {
                if (comparator.compare((T) array[j], (T) array[j + 1]) > 0) {
                    T temp = (T) array[j];
                    array[j] = array[j + 1];
                    array[j + 1] = temp;
                }
            }
        }
    }

    @Override
    public void unique() {
        int uniqueSize = 0;
        for (int i = 0; i < size; i++) {
            boolean isDuplicate = false;
            for (int j = 0; j < uniqueSize; j++) {
                if (array[i].equals(array[j])) {
                    isDuplicate = true;
                    break;
                }
            }
            if (!isDuplicate) {
                array[uniqueSize++] = array[i];
            }
        }
        for (int i = uniqueSize; i < size; i++) {
            array[i] = null;
        }
        size = uniqueSize;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("[");
        for (int i = 0; i < size; i++) {
            sb.append(array[i]);
            if (i < size - 1) sb.append(", ");
        }
        sb.append("]");
        return sb.toString();
    }

    @Override
    public Iterator<T> iterator() {
        return new Iterator<T>() {
            private int currentIndex = 0;

            @Override
            public boolean hasNext() {
                return currentIndex < size;
            }

            @Override
            public T next() {
                if (!hasNext()) throw new NoSuchElementException();
                return (T) array[currentIndex++];
            }
        };
    }

    // Methods not implemented but required by the List interface
    @Override
    public boolean isEmpty() { return size == 0; }

    @Override
    public boolean contains(Object o) { throw new UnsupportedOperationException(); }

    @Override
    public Object[] toArray() { throw new UnsupportedOperationException(); }

    @Override
    public <T1> T1[] toArray(T1[] a) { throw new UnsupportedOperationException(); }

    @Override
    public boolean containsAll(java.util.Collection<?> c) { throw new UnsupportedOperationException(); }

    @Override
    public boolean addAll(java.util.Collection<? extends T> c) { throw new UnsupportedOperationException(); }

    @Override
    public boolean addAll(int index, java.util.Collection<? extends T> c) { throw new UnsupportedOperationException(); }

    @Override
    public boolean removeAll(java.util.Collection<?> c) { throw new UnsupportedOperationException(); }

    @Override
    public boolean retainAll(java.util.Collection<?> c) { throw new UnsupportedOperationException(); }

    @Override
    public void clear() { throw new UnsupportedOperationException(); }

    @Override
    public T set(int index, T element) { throw new UnsupportedOperationException(); }

    @Override
    public int indexOf(Object o) { throw new UnsupportedOperationException(); }

    @Override
    public int lastIndexOf(Object o) { throw new UnsupportedOperationException(); }

    @Override
    public java.util.ListIterator<T> listIterator() { throw new UnsupportedOperationException(); }

    @Override
    public java.util.ListIterator<T> listIterator(int index) { throw new UnsupportedOperationException(); }

    @Override
    public java.util.List<T> subList(int fromIndex, int toIndex) { throw new UnsupportedOperationException(); }
}
