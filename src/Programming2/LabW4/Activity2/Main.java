package Programming2.LabW4.Activity2;

import Programming2.LabW4.Activity2.Products.CleaningProduct;
import Programming2.LabW4.Activity2.Products.ElectronicProduct;
import Programming2.LabW4.Activity2.Products.FoodProduct;

public class Main {
    public static void main(String[] args) {
        Warehouse<FoodProduct> foodWarehouse = new Warehouse<>();
        Warehouse<CleaningProduct> cleaningWarehouse = new Warehouse<>();
        Warehouse<ElectronicProduct> electronicWarehouse = new Warehouse<>();
        Warehouse<FurnitureProduct> furnitureWarehouse = new Warehouse<>();

        FoodProduct apple = new FoodProduct("1", "Apple", "Fruit", 0.5, "2024-12-31");
        FoodProduct banana = new FoodProduct("2", "Banana", "Fruit", 0.3, "2024-11-30");
        FoodProduct carrot = new FoodProduct("3", "Carrot", "Vegetable", 0.7, "2024-10-20");
        CleaningProduct detergent = new CleaningProduct("4", "Detergent", "Cleaning", 3.5, "Detergent Inc.");
        ElectronicProduct laptop = new ElectronicProduct("5", "Laptop", "Electronics", 1000.0, "Dell", 2);
        FurnitureProduct chair = new FurnitureProduct("6", "Chair", "Furniture", 150.0, "Wood", "Brown");

        foodWarehouse.addProduct(apple);
        foodWarehouse.addProduct(banana);
        foodWarehouse.addProduct(carrot);
        cleaningWarehouse.addProduct(detergent);
        electronicWarehouse.addProduct(laptop);
        furnitureWarehouse.addProduct(chair);

        System.out.println("All food products:");
        System.out.println(foodWarehouse);

        System.out.println("\nFood products in category 'Fruit':");
        System.out.println(Warehouse.listToString(foodWarehouse.findProductsByCategory("Fruit")));

        System.out.println("\nFinding food product with ID '3':");
        System.out.println(foodWarehouse.findProductById("3"));

        foodWarehouse.sortProductsByPrice();
        System.out.println("\nFood products sorted by price:");
        System.out.println(foodWarehouse);

        foodWarehouse.removeProduct("2");
        System.out.println("\nFood products after removing product with ID '2':");
        System.out.println(foodWarehouse);
    }
}