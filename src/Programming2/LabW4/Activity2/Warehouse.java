package Programming2.LabW4.Activity2;

import Programming2.LabW4.Activity2.Products.Product;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;

public class Warehouse<T extends Product> {
    private List<T> products;

    public Warehouse() {
        this.products = new ArrayList<>();
    }

    public void addProduct(T product) {
        products.add(product);
    }

    public boolean removeProduct(String productId) {
        return products.removeIf(product -> product.getId().equals(productId));
    }

    public T findProductById(String productId) {
        return products.stream()
                .filter(product -> product.getId().equals(productId))
                .findFirst()
                .orElse(null);
    }

    public List<T> findProductsByCategory(String category) {
        List<T> categoryProducts = new ArrayList<>();
        for (T product : products) {
            if (product.getCategory().equalsIgnoreCase(category)) {
                categoryProducts.add(product);
            }
        }
        return categoryProducts;
    }

    public void sortProductsByName() {
        products.sort(Comparator.naturalOrder());
    }

    public void sortProductsByPrice() {
        products.sort(Comparator.comparingDouble(Product::getPrice));
    }

    public static <T extends Product> String listToString(List<T> productList) {
        StringBuilder sb = new StringBuilder("[\n");
        for (T product : productList) {
            sb.append(product).append("\n");
        }
        sb.append("]");
        return sb.toString();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder("Warehouse{products=\n");
        for (T product : products) {
            sb.append(product).append("\n");
        }
        sb.append("}");
        return sb.toString();
    }
}