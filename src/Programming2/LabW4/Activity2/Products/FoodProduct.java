package Programming2.LabW4.Activity2.Products;

public class FoodProduct extends Product {
    private String expiryDate;

    public FoodProduct(String id, String name, String category, double price, String expiryDate) {
        super(id, name, category, price);
        this.expiryDate = expiryDate;
    }

    public String getExpiryDate() {
        return expiryDate;
    }

    @Override
    public String toString() {
        return "FoodProduct{expiryDate='" + expiryDate + "', " + super.toString() + "}";
    }
}