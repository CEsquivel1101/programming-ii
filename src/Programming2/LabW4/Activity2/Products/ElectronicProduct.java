package Programming2.LabW4.Activity2.Products;

public class ElectronicProduct extends Product {
    private String brand;
    private int warrantyPeriod;

    public ElectronicProduct(String id, String name, String category, double price, String brand, int warrantyPeriod) {
        super(id, name, category, price);
        this.brand = brand;
        this.warrantyPeriod = warrantyPeriod;
    }

    public String getBrand() {
        return brand;
    }

    public int getWarrantyPeriod() {
        return warrantyPeriod;
    }

    @Override
    public String toString() {
        return "ElectronicProduct{" +
                "brand='" + brand + '\'' +
                ", warrantyPeriod=" + warrantyPeriod +
                ", " + super.toString() +
                '}';
    }
}
