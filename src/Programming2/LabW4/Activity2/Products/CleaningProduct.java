package Programming2.LabW4.Activity2.Products;

public class CleaningProduct extends Product {
    private String manufacturer;

    public CleaningProduct(String id, String name, String category, double price, String manufacturer) {
        super(id, name, category, price);
        this.manufacturer = manufacturer;
    }

    public String getManufacturer() {
        return manufacturer;
    }

    @Override
    public String toString() {
        return "CleaningProduct{" +
                "manufacturer='" + manufacturer + '\'' +
                ", " + super.toString() +
                '}';
    }
}
