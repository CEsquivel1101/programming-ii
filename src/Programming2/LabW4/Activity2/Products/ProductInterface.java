package Programming2.LabW4.Activity2.Products;

public interface ProductInterface {
    String getId();
    String getName();
    String getCategory();
    double getPrice();
}
