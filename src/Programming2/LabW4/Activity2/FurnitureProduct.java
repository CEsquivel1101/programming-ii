package Programming2.LabW4.Activity2;

import Programming2.LabW4.Activity2.Products.Product;

public class FurnitureProduct extends Product {
    private String material;
    private String color;

    public FurnitureProduct(String id, String name, String category, double price, String material, String color) {
        super(id, name, category, price);
        this.material = material;
        this.color = color;
    }

    public String getMaterial() {
        return material;
    }

    public String getColor() {
        return color;
    }

    @Override
    public String toString() {
        return "FurnitureProduct{" +
                "material='" + material + '\'' +
                ", color='" + color + '\'' +
                ", " + super.toString() +
                '}';
    }
}