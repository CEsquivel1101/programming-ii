package Programming2.LabW5.Activity2;

import java.util.LinkedHashSet;
import java.util.Set;

public class Centralizer {
    private Set<Station> stations;

    public Centralizer() {
        this.stations = new LinkedHashSet<>();
    }

    public void add(Station station) {
        stations.add(station);
    }

    public void iterate() {
        Set<Person> uniquePeople = new LinkedHashSet<>();

        for (Station station : stations) {
            uniquePeople.addAll(station.getRegisteredPeople());
        }

        for (Station station : stations) {
            System.out.println("Data from " + station);
            for (Person person : station.getRegisteredPeople()) {
                System.out.println(person);
            }
            System.out.println(); // Blank line to separate stations
        }

        System.out.println("Processed Data:");
        for (Person person : uniquePeople) {
            System.out.println(person);
        }
    }
}