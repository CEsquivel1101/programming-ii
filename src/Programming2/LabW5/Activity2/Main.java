package Programming2.LabW5.Activity2;

public class Main {
    public static void main(String[] args) {
        Station c1 = new Station("Station 1");
        c1.add(new Person("Juan", 1252, 18));
        c1.add(new Person("Raul", 2252, 18));
        c1.add(new Person("Janeth", 1587, 18));
        c1.add(new Person("Janeth", 1587, 18)); // Duplicate

        Station c2 = new Station("Station 2");
        c2.add(new Person("Juan", 1252, 18)); // Duplicate
        c2.add(new Person("Mery", 3225, 21));
        c2.add(new Person("Jenny", 2557, 18));
        c2.add(new Person("Janeth", 1587, 18)); // Duplicate

        Station c3 = new Station("Station 3");
        c3.add(new Person("Julio", 2222, 18));
        c3.add(new Person("Alex", 2558, 18));
        c3.add(new Person("Janeth", 1587, 18)); // Duplicate

        Centralizer centralizer = new Centralizer();
        centralizer.add(c1);
        centralizer.add(c2);
        centralizer.add(c3);

        centralizer.iterate();
    }
}