package Programming2.LabW5.Activity2;

import java.util.LinkedHashSet;
import java.util.Set;

public class Station {
    private String name;
    private Set<Person> registeredPeople;

    public Station(String name) {
        this.name = name;
        this.registeredPeople = new LinkedHashSet<>();
    }

    public void add(Person person) {
        registeredPeople.add(person);
    }

    public Set<Person> getRegisteredPeople() {
        return registeredPeople;
    }

    @Override
    public String toString() {
        return "Station: " + name;
    }
}