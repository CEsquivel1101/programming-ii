package Programming2.LabW5.Activity3;
public class Main {
    public static void main(String[] args) {
        TaskManager manager = new TaskManager();

        // Add tasks to the manager
        manager.addTask(new Task("class 1"));
        manager.addTask(new Task("solve quiz"));
        manager.addTask(new Task("attend client"));
        manager.addTask(new Task("attend interview"));

        // Initial display
        System.out.println("Initial Tasks:");
        manager.displayCurrentStatus();

        // Process tasks one by one and display the state after each step
        System.out.println("Processing tasks:");
        do {
            manager.processNextTask();
        } while (manager.hasMoreTasks());
    }
}
