package Programming2.LabW5.Activity3;

public enum TaskStatus {
    PENDING,
    IN_PROGRESS,
    COMPLETED
}
