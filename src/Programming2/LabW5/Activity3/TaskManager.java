package Programming2.LabW5.Activity3;

import java.util.ArrayList;
import java.util.List;

public class TaskManager {
    private List<Task> tasks;
    private int currentIndex;

    public TaskManager() {
        this.tasks = new ArrayList<>();
        this.currentIndex = -1;
    }

    public void addTask(Task task) {
        tasks.add(task); // Add to the list
    }

    public void processNextTask() {
        if (currentIndex >= 0) {
            tasks.get(currentIndex).setStatus(TaskStatus.COMPLETED);
        }
        currentIndex++;
        if (currentIndex < tasks.size()) {
            tasks.get(currentIndex).setStatus(TaskStatus.IN_PROGRESS);
        }
        displayCurrentStatus();
    }

    public void displayCurrentStatus() {
        System.out.println();
        for (Task task : tasks) {
            System.out.println(task.getDescription() + ", " + task.getStatus().toString().toLowerCase());
        }
        System.out.println();
    }

    public boolean hasMoreTasks() {
        return currentIndex < tasks.size();
    }
}
