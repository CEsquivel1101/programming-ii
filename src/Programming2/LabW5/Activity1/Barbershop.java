package Programming2.LabW5.Activity1;

import java.util.LinkedList;
import java.util.Queue;

public class Barbershop {
    private Barber[] barbers;
    private Queue<Client> waitingClients;
    private Queue<Client> inServiceClients;
    private Queue<Client> allClients;

    public Barbershop(Barber[] barbers) {
        this.barbers = barbers;
        this.waitingClients = new LinkedList<>();
        this.inServiceClients = new LinkedList<>();
        this.allClients = new LinkedList<>();
    }

    // Getter for waitingClients
    public Queue<Client> getWaitingClients() {
        return waitingClients;
    }

    // Getter for inServiceClients
    public Queue<Client> getInServiceClients() {
        return inServiceClients;
    }

    public void addClient(Client client) {
        waitingClients.offer(client);  // Enqueue
        allClients.offer(client);      // Track all clients
    }

    public void updateStatus() {
        // Move in-service clients to served status
        while (!inServiceClients.isEmpty()) {
            Client client = inServiceClients.poll();
            client.setStatus(ClientStatus.SERVED);
        }

        // Move waiting clients to in-service status
        while (inServiceClients.size() < barbers.length && !waitingClients.isEmpty()) {
            Client client = waitingClients.poll();  // Dequeue
            client.setStatus(ClientStatus.IN_SERVICE);
            inServiceClients.offer(client);
        }

        printCurrentStatus();
    }

    private void printCurrentStatus() {
        System.out.println("\n--- Current Status ---");

        for (Client client : allClients) {
            System.out.println(client);
        }

        if (waitingClients.isEmpty() && inServiceClients.isEmpty()) {
            System.out.println("No more clients to attend.");
        }
    }
}
