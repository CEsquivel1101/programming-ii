package Programming2.LabW5.Activity1;

public class Main {
    public static void main(String[] args) {
        Barber[] barbers = {
                new Barber("Julio"),
                new Barber("Tulio"),
        };

        Barbershop barbershop = new Barbershop(barbers);

        Client[] clients = {
                new Client("c1"), new Client("c2"), new Client("c3"),
                new Client("c4"), new Client("c5"), new Client("c6"),
                new Client("c7"), new Client("c8")
        };

        for (Client client : clients) {
            barbershop.addClient(client);
        }

        while (!barbershop.getWaitingClients().isEmpty() || !barbershop.getInServiceClients().isEmpty()) {
            barbershop.updateStatus();
        }
    }
}