package Programming2.LabW5.Activity1;

class Client extends Person {
    private ClientStatus status;

    public Client(String name) {
        super(name);
        this.status = ClientStatus.WAITING;
    }

    public ClientStatus getStatus() {
        return status;
    }

    public void setStatus(ClientStatus status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return name + ", " + status.toString().toLowerCase().replace('_', ' ');
    }
}