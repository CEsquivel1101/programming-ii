package Programming2.LabW5.Activity1;

enum ClientStatus {
    WAITING,
    IN_SERVICE,
    SERVED
}