package Programming2.LabW8.Activity1.single.responsibility;

public class Address {
    private String country;
    private String city;
    private String street;
    private int no;

    public Address(String country, String city, String street, int no) {
        this.country = country;
        this.city = city;
        this.street = street;
        this.no = no;
    }

    public void updateAddress(String country, String city, String street, int no) {
        this.country = country;
        this.city = city;
        this.street = street;
        this.no = no;
    }

    @Override
    public String toString() {
        return "Address{" +
                "country='" + country + '\'' +
                ", city='" + city + '\'' +
                ", street='" + street + '\'' +
                ", no=" + no +
                '}';
    }
}
