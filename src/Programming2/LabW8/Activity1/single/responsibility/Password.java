package Programming2.LabW8.Activity1.single.responsibility;
public class Password {
    private String password;

    public Password(String password) {
        this.password = password;
    }

    public boolean authenticate(String inputPassword) {
        return this.password.equals(inputPassword);
    }

    public void updatePassword(String newPassword) {
        this.password = newPassword;
    }

    @Override
    public String toString() {
        return "Password{********}";
    }
}