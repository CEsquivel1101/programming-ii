package Programming2.LabW8.Activity1.single.responsibility;
public class User {
    private String firstName;
    private String lastName;
    private Address address;
    private String user;
    private Password password;
    private EmailService emailService;

    public User(String firstName, String lastName, Address address, String user, Password password, EmailService emailService) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
        this.user = user;
        this.password = password;
        this.emailService = emailService;
    }

    public void updateAddress(String country, String city, String street, int no) {
        this.address.updateAddress(country, city, street, no);
    }

    public void updatePassword(String password) {
        this.password.updatePassword(password);
    }

    public void sendEmail(String message) {
        this.emailService.sendEmail(message);
    }

    @Override
    public String toString() {
        return "User{" +
                "firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", address=" + address +
                ", user='" + user + '\'' +
                ", password=" + password +
                ", emailService=" + emailService +
                '}';
    }
}