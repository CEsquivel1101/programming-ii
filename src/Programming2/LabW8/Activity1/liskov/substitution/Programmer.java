package Programming2.LabW8.Activity1.liskov.substitution;


public class Programmer extends Employee {
    private String programmingLanguage;

    public Programmer(String name, int baseSalary, String programmingLanguage) {
        super(name, baseSalary);
        this.programmingLanguage = programmingLanguage;
    }

    @Override
    public int calculatePayment() {
        return baseSalary + 500;
    }
}
