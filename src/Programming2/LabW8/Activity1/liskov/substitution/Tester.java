package Programming2.LabW8.Activity1.liskov.substitution;

public class Tester extends Employee {
    private TestType testType;

    public Tester(String name, int baseSalary, TestType testType) {
        super(name, baseSalary);
        this.testType = testType;
    }

    @Override
    public int calculatePayment() {
        int bonus = 0;
        if (this.testType == TestType.AUTOMATED) {
            bonus = 300;
        }
        return baseSalary + bonus;
    }
}