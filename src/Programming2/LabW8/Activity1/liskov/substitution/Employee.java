package Programming2.LabW8.Activity1.liskov.substitution;

public abstract class Employee {
    protected String name;
    protected int baseSalary;

    public Employee(String name, int baseSalary) {
        this.name = name;
        this.baseSalary = baseSalary;
    }

    public abstract int calculatePayment();
}
