package Programming2.LabW8.Activity1.open.close;

public interface Instrument {
    void playNotes(int n);
}