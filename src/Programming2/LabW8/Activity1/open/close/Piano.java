package Programming2.LabW8.Activity1.open.close;

public class Piano implements Instrument {
    public void play() {
        System.out.println("Piano is playing...");
    }

    @Override
    public void playNotes(int n) {
        for (int i = 0; i < n; i++) {
            play();
        }
    }
}