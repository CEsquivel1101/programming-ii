package Programming2.LabW8.Activity1.open.close;

public class Drum implements Instrument {
    public void play() {
        System.out.println("Drum is playing...");
    }

    @Override
    public void playNotes(int n) {
        for (int i = 0; i < n; i++) {
            play();
        }
    }
}