package Programming2.LabW8.Activity1;


import Programming2.LabW8.Activity1.dependency.inversion.DataBaseManager;
import Programming2.LabW8.Activity1.dependency.inversion.MongoDB;
import Programming2.LabW8.Activity1.dependency.inversion.MySql;
import Programming2.LabW8.Activity1.interfaces.segregation.CheapCamera;
import Programming2.LabW8.Activity1.interfaces.segregation.SmartCamera;
import Programming2.LabW8.Activity1.liskov.substitution.Employee;
import Programming2.LabW8.Activity1.liskov.substitution.Programmer;
import Programming2.LabW8.Activity1.liskov.substitution.TestType;
import Programming2.LabW8.Activity1.liskov.substitution.Tester;
import Programming2.LabW8.Activity1.open.close.Drum;
import Programming2.LabW8.Activity1.open.close.Guitar;
import Programming2.LabW8.Activity1.open.close.Instrument;
import Programming2.LabW8.Activity1.open.close.Piano;
import Programming2.LabW8.Activity1.single.responsibility.Address;
import Programming2.LabW8.Activity1.single.responsibility.EmailService;
import Programming2.LabW8.Activity1.single.responsibility.Password;
import Programming2.LabW8.Activity1.single.responsibility.User;
import Programming2.LabW8.Activity1.utils.Utils;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    public static void showMenu() {
        System.out.println("*** SOLID ***");
        System.out.println("1. Single responsibility");
        System.out.println("2. Open close");
        System.out.println("3. Liskov substitution");
        System.out.println("4. Interface segregation");
        System.out.println("5. Dependency inversion");
        System.out.println("6. Exit");
    }

    public static void updateUser() {
        Address address = new Address("USA", "Pasadena", "5th avenue", 20);
        Password password = new Password("123");
        EmailService emailService = new EmailService("jhon.doe@mail.com");
        User user = new User("Jhon", "Doe", address, "jhon.doe", password, emailService);

        System.out.println("Update address for Jhon Doe");
        System.out.println("Country: ");
        String country = Utils.readString();
        System.out.println("City: ");
        String city = Utils.readString();
        System.out.println("Av.: ");
        String avenue = Utils.readString();
        System.out.println("No.: ");
        int no = Utils.readInt();
        System.out.println("Enter Jhon passoword to continue:");
        String inputPassword = Utils.readString();
        if (password.authenticate(inputPassword)) {
            user.updateAddress(country, city, avenue, no);
            System.out.println(user);
        } else {
            System.out.println("Wrong password");
        }
    }

    public static void playSymphony() {
        List<Instrument> instruments = List.of(new Guitar(), new Piano(), new Drum());
        int[] notes = new int[]{2, 3, 4};
        for (int note : notes) {
            for (Instrument instrument : instruments) {
                instrument.playNotes(note);
            }
        }
    }

    public static void calculatePayments() {
        Tester[] testers = new Tester[]{
                new Tester("Joan", 1000, TestType.MANUAL),
                new Tester("Carmen", 1000, TestType.AUTOMATED)};
        Programmer[] programmers = new Programmer[]{
                new Programmer("Aida", 1000, "C#"),
                new Programmer("Julia", 1000, "Java")};

        List<Employee> employees = new ArrayList<>();
        employees.addAll(Arrays.asList(testers));
        employees.addAll(Arrays.asList(programmers));
        int totalPayment = 0;
        for (Employee emp : employees) {
            totalPayment += emp.calculatePayment();
        }
        System.out.println("Total payment employees: " + totalPayment);
    }

    public static void testDatabase() {
        DataBaseManager controller = new DataBaseManager(new MySql());
        controller.executeQuery("SELECT * FROM PRODUCTS;");
        controller = new DataBaseManager(new MongoDB());
        controller.dump();
        controller.poll();
    }

    public static void cameraFunction() throws Exception {
        SmartCamera camera = new SmartCamera();
        camera.detectMovement();
        camera.faceRecognition();
        camera.recordVideo();
        camera.zoomIn();

        CheapCamera cheapCam = new CheapCamera();
        cheapCam.takePhoto();
        cheapCam.recordVideo();
    }

    public static void handleMenu() throws Exception {
        int option = 0;

        while (option != 6) {
            showMenu();
            System.out.println("Choose option: ");
            option = Utils.readInt();
            switch (option) {
                case 1:
                    updateUser();
                    break;
                case 2:
                    playSymphony();
                    break;
                case 3:
                    calculatePayments();
                    break;
                case 4:
                    cameraFunction();
                    break;
                case 5:
                    testDatabase();
                    break;
                case 6:
                    break;
                default:
                    System.out.println("Wrong option");
                    break;
            }
        }
    }

    public static void main(String[] args) throws Exception {
        handleMenu();
        Utils.close();
    }
}
