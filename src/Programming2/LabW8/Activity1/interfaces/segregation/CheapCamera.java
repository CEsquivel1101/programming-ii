package Programming2.LabW8.Activity1.interfaces.segregation;
public class CheapCamera implements ICamera {
    @Override
    public void takePhoto() {
        System.out.println("Taking a photo with CheapCamera...");
    }

    @Override
    public void recordVideo() {
        System.out.println("Recording a video with CheapCamera...");
    }
}