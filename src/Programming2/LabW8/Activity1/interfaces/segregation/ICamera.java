package Programming2.LabW8.Activity1.interfaces.segregation;

public interface ICamera {
    void takePhoto();
    void recordVideo();
}