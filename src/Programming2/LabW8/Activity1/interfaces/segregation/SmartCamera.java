package Programming2.LabW8.Activity1.interfaces.segregation;
public class SmartCamera implements AdvancedCamera {
    @Override
    public void detectMovement() throws Exception {
        System.out.println("Detecting movement with SmartCamera...");
    }

    @Override
    public void thermalDetection() throws Exception {
        System.out.println("Thermal detection with SmartCamera...");
    }

    @Override
    public void faceRecognition() throws Exception {
        System.out.println("Face recognition with SmartCamera...");
    }

    @Override
    public void zoomIn() {
        System.out.println("Zooming in with SmartCamera...");
    }

    @Override
    public void takePhoto() {
        System.out.println("Taking a photo with SmartCamera...");
    }

    @Override
    public void recordVideo() {
        System.out.println("Recording a video with SmartCamera...");
    }
}