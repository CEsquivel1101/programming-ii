package Programming2.LabW8.Activity1.interfaces.segregation;

public interface AdvancedCamera extends ICamera {
    void detectMovement() throws Exception;
    void thermalDetection() throws Exception;
    void faceRecognition() throws Exception;
    void zoomIn();
}