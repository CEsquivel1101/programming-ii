package Programming2.LabW8.Activity1.dependency.inversion;

public class MySql implements Database {
    @Override
    public void executeQuery(String query) {
        System.out.println("Executing MySql query: " + query);
    }

    @Override
    public void dump() {
        System.out.println("MySql database dump...");
    }

    @Override
    public void poll() {
        System.out.println("MySql database poll...");
    }
}