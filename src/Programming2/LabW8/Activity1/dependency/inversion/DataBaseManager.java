package Programming2.LabW8.Activity1.dependency.inversion;

public final class DataBaseManager {
    private Database database;

    public DataBaseManager(Database database) {
        this.database = database;
    }

    public void executeQuery(String query) {
        database.executeQuery(query);
    }

    public void dump() {
        database.dump();
    }

    public void poll() {
        database.poll();
    }
}