package Programming2.LabW8.Activity1.dependency.inversion;

public interface Database {
    void executeQuery(String query);
    void dump();
    void poll();
}
