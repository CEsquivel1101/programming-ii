package Programming2.LabW8.Activity1.dependency.inversion;

public class MongoDB implements Database {
    @Override
    public void executeQuery(String query) {
        System.out.println("Executing MongoDB query: " + query);
    }

    @Override
    public void dump() {
        System.out.println("MongoDB database dump...");
    }

    @Override
    public void poll() {
        System.out.println("MongoDB database poll...");
    }
}
