package Programming2.LabW8.Activity2;

import java.util.HashMap;
import java.util.Map;

public class Student {
    private int id;
    private String name;
    private Map<String, Double> scores = new HashMap<>();

    public Student(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public void addScore(String subject, double score) {
        scores.put(subject, score);
    }

    public Double getScore(String subject) {
        return scores.get(subject);
    }

    public double calculateAverage() {
        return scores.values().stream().mapToDouble(Double::doubleValue).average().orElse(0.0);
    }
}
