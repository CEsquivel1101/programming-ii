package Programming2.LabW8.Activity2;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    private static List<Student> students = new ArrayList<>();
    private static List<Subject> subjects = new ArrayList<>();
    private static int studentIdCounter = 1;

    public static void main(String[] args) {
        initializeData();

        Scanner sc = new Scanner(System.in);
        int option = 0;

        while (option != 6) {
            showMenu();
            option = Integer.parseInt(sc.nextLine());

            switch (option) {
                case 1:
                    addNewStudent(sc);
                    break;
                case 2:
                    listStudents();
                    break;
                case 3:
                    addNewSubject(sc);
                    break;
                case 4:
                    editScores(sc);
                    break;
                case 5:
                    viewStudent(sc);
                    break;
                default:
                    System.out.println("Exiting program...");
                    break;
            }
        }
    }

    private static void initializeData() {
        subjects.add(new Subject("Programming"));
        subjects.add(new Subject("DataBase"));
        subjects.add(new Subject("Language"));

        students.add(new Student(studentIdCounter++, "Gery"));
        students.add(new Student(studentIdCounter++, "Joel"));
        students.add(new Student(studentIdCounter++, "Carmen"));

        students.get(0).addScore("Programming", 67.0);
        students.get(0).addScore("DataBase", 70.0);
        students.get(0).addScore("Language", 69.0);

        students.get(1).addScore("Programming", 80.0);
        students.get(1).addScore("DataBase", 85.0);
        students.get(1).addScore("Language", 85.0);

        students.get(2).addScore("Programming", 90.5);
        students.get(2).addScore("DataBase", 65.5);
        students.get(2).addScore("Language", 95.5);
    }

    private static void showMenu() {
        System.out.println("*** Student Management ***");
        System.out.println("1. New student");
        System.out.println("2. List students");
        System.out.println("3. Add subject");
        System.out.println("4. Edit scores");
        System.out.println("5. View student");
        System.out.println("6. Exit");
    }

    private static void addNewStudent(Scanner sc) {
        System.out.print("Insert name: ");
        Student newStudent = new Student(studentIdCounter++, sc.nextLine());
        students.add(newStudent);
        subjects.forEach(subject -> newStudent.addScore(subject.getName(), 0.0));
        System.out.println("Student added successfully. ID: " + newStudent.getId());
    }

    private static void listStudents() {
        if (students.isEmpty()) {
            System.out.println("No students available.");
            return;
        }

        students.forEach(student -> {
            System.out.println("ID: " + student.getId() + " Name: " + student.getName());
            subjects.forEach(subject ->
                    System.out.println(subject.getName() + ": " + student.getScore(subject.getName()))
            );
        });
    }

    private static void addNewSubject(Scanner sc) {
        System.out.print("Subject name: ");
        String newSubjectName = sc.nextLine();
        subjects.add(new Subject(newSubjectName));
        students.forEach(student -> student.addScore(newSubjectName, 0.0));
        System.out.println("Subject added successfully.");
    }

    private static void editScores(Scanner sc) {
        try {
            System.out.print("Select student ID: ");
            int studentId = Integer.parseInt(sc.nextLine());
            Student student = findStudentById(studentId);

            if (student != null) {
                System.out.println("Editing scores for " + student.getName());
                for (Subject subject : subjects) {
                    System.out.println(subject.getName() + " Current score: " + student.getScore(subject.getName()));
                    System.out.print("New score: ");
                    double newScore = Double.parseDouble(sc.nextLine());
                    student.addScore(subject.getName(), newScore);
                }
                System.out.println("Scores updated successfully.");
            } else {
                System.out.println("Student not found.");
            }
        } catch (NumberFormatException e) {
            System.out.println("Invalid input. Please enter a valid number.");
        }
    }

    private static void viewStudent(Scanner sc) {
        try {
            System.out.print("Select student ID: ");
            int studentId = Integer.parseInt(sc.nextLine());
            Student student = findStudentById(studentId);

            if (student != null) {
                System.out.println("Name: " + student.getName());
                subjects.forEach(subject ->
                        System.out.println(subject.getName() + ": " + student.getScore(subject.getName()))
                );
                System.out.println("Average: " + student.calculateAverage());
            } else {
                System.out.println("Student not found.");
            }
        } catch (NumberFormatException e) {
            System.out.println("Invalid input. Please enter a valid number.");
        }
    }

    private static Student findStudentById(int id) {
        for (Student student : students) {
            if (student.getId() == id) {
                return student;
            }
        }
        return null;
    }
}