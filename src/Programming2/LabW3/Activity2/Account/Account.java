package Programming2.LabW3.Activity2.Account;

public abstract class Account {
    protected double balance;

    public Account(double balance) {
        this.balance = balance;
    }

    public double getBalance() {
        return balance;
    }

    public abstract void withdraw(double amount);

    @Override
    public String toString() {
        return "Balance: " + balance;
    }
}
