package Programming2.LabW3.Activity2.Account;

public class TemporaryAccount extends Account {
    private boolean enabled = true;

    public TemporaryAccount(double balance) {
        super(balance);
    }

    @Override
    public void withdraw(double amount) {
        if (!enabled) {
            throw new IllegalStateException("The account is disabled");
        }
        if (balance >= amount) {
            balance -= amount;
            if (balance == 0) {
                disable();
            }
        } else {
            throw new IllegalArgumentException("Insufficient funds in the temporary account");
        }
    }

    public void disable() {
        enabled = false;
    }

    public boolean isEnabled() {
        return enabled;
    }

    @Override
    public String toString() {
        return "Balance: " + balance + ", Enabled: " + enabled;
    }
}