package Programming2.LabW3.Activity2;

import Programming2.LabW3.Activity2.Account.CheckingAccount;
import Programming2.LabW3.Activity2.Account.TemporaryAccount;
import Programming2.LabW3.Activity2.Card.CreditCard;
import Programming2.LabW3.Activity2.Card.PrepaidCard;

public class Main {
    public static void main(String[] args) {
        // Example 1: Checking account with default credit card limit
        CheckingAccount t1 = new CheckingAccount(500);
        CreditCard c1 = new CreditCard(); // Default limit of 500
        c1.set(t1);
        c1.withdraw(50);
        System.out.println(c1);  // Expected: CreditCard limit: 450.0, Account balance: 500.0

        // Example 2: Checking account with manual credit card limit
        CheckingAccount t2 = new CheckingAccount(100);
        CreditCard c2 = new CreditCard(500); // Manual limit of 500
        c2.set(t2);
        c2.withdraw(50);
        System.out.println(c2);  // Expected: CreditCard limit: 450.0, Account balance: 100.0

        // Example 3: Temporary account with prepaid card
        TemporaryAccount t3 = new TemporaryAccount(100);
        PrepaidCard p1 = new PrepaidCard(100);
        p1.set(t3);
        p1.withdraw(100);
        System.out.println(p1);  // Expected: PrepaidCard limit: 0.0, Account balance: 0.0, Enabled: false

        System.out.println();
        System.out.println("Extra Exercises");
        System.out.println();


        // Example 4: Checking account with default credit card limit
        CheckingAccount t4 = new CheckingAccount(2000);
        CreditCard c4 = new CreditCard(1000); // Default limit of 1000
        c4.set(t4);
        c4.withdraw(500);
        System.out.println(c4);  // Expected: CreditCard limit: 500.0, Account balance: 2000.0

        // Additional Examples
        // Check remaining limit on credit card after multiple withdrawals
        c1.withdraw(200);
        System.out.println("Remaining limit of c1 after withdrawing 200: " + c1.getAvailableLimit());

        // Check status of temporary account after partial withdrawal
        TemporaryAccount t5 = new TemporaryAccount(150);
        PrepaidCard p2 = new PrepaidCard(150);
        p2.set(t5);
        p2.withdraw(50);
        System.out.println(p2);

        // Try to withdraw an amount that exceeds the limit on a prepaid card
        try {
            p2.withdraw(200);
        } catch (IllegalArgumentException e) {
            System.out.println(e.getMessage());
        }
    }
}
