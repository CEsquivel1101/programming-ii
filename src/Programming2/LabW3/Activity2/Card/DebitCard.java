package Programming2.LabW3.Activity2.Card;

import Programming2.LabW3.Activity2.Account.CheckingAccount;

public class DebitCard extends Card {
    @Override
    public void withdraw(double amount) {
        if (account instanceof CheckingAccount) {
            account.withdraw(amount);
        } else {
            throw new IllegalStateException("Debit card can only be associated with a checking account");
        }
    }

    @Override
    public String toString() {
        return "DebitCard, Account balance: " + account.getBalance();
    }
}
