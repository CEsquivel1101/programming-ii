package Programming2.LabW3.Activity2.Card;

import Programming2.LabW3.Activity2.Account.Account;

public abstract class Card {
    protected Account account;

    public void set(Account account) {
        this.account = account;
    }

    public abstract void withdraw(double amount);
}
