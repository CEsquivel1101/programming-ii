package Programming2.LabW3.Activity2.Card;

import Programming2.LabW3.Activity2.Account.CheckingAccount;

public class CreditCard extends Card {
    private double limit;

    // Constructor with default limit
    public CreditCard() {
        this.limit = 500;  // Default limit
    }

    // Constructor with specified limit
    public CreditCard(double limit) {
        this.limit = limit;
    }

    @Override
    public void withdraw(double amount) {
        if (account instanceof CheckingAccount) {
            if (amount <= limit) {
                limit -= amount;
            } else {
                throw new IllegalArgumentException("Amount exceeds the credit card limit");
            }
        } else {
            throw new IllegalStateException("Credit card can only be associated with a checking account");
        }
    }

    public double getAvailableLimit() {
        return limit;
    }

    @Override
    public String toString() {
        return "CreditCard limit: " + limit + ", Account balance: " + account.getBalance();
    }
}
