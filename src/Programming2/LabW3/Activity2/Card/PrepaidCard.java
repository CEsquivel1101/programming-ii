package Programming2.LabW3.Activity2.Card;

import Programming2.LabW3.Activity2.Account.TemporaryAccount;

public class PrepaidCard extends Card {
    private double limit;

    public PrepaidCard(double limit) {
        this.limit = limit;
    }

    @Override
    public void withdraw(double amount) {
        if (account instanceof TemporaryAccount) {
            if (amount <= limit) {
                account.withdraw(amount);
                limit -= amount;
            } else {
                throw new IllegalArgumentException("Amount exceeds the prepaid card limit");
            }
        } else {
            throw new IllegalStateException("Prepaid card can only be associated with a temporary account");
        }
    }

    public double getAvailableLimit() {
        return limit;
    }

    public boolean isAccountEnabled() {
        if (account instanceof TemporaryAccount) {
            return ((TemporaryAccount) account).isEnabled();
        }
        throw new IllegalStateException("Prepaid card can only be associated with a temporary account");
    }

    @Override
    public String toString() {
        return "PrepaidCard limit: " + limit + ", Account balance: " + account.getBalance() + ", Account is enabled: " + ((TemporaryAccount) account).isEnabled();
    }
}
