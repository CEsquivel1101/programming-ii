package Programming2.LabW3.Activity1;

public class SpecialCard extends Card {
    private String specialPower;

    public SpecialCard(Suit suit, String value, String specialPower) {
        super(suit, value);
        this.specialPower = specialPower;
    }

    public String getSpecialPower() {
        return specialPower;
    }

    @Override
    public String toString() {
        return super.toString() + " (" + specialPower + ")";
    }
}
