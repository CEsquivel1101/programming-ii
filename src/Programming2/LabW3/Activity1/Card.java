package Programming2.LabW3.Activity1;

public class Card {
    private Suit suit;
    private String value;

    public Card(Suit suit, String value) {
        this.suit = suit;
        this.value = value;
    }

    public Suit getSuit() {
        return suit;
    }

    public String getValue() {
        return value;
    }

    @Override
    public String toString() {
        return suit.getSymbol() + value;
    }

    // Method to compare card values
    public int compareValue(Card other) {
        String order = "23456789TJQKA";
        return Integer.compare(order.indexOf(this.value), order.indexOf(other.value));
    }

    // Method to determine the result of comparing two cards
    public String determineWinner(Card other) {
        int result = compareValue(other);
        if (result > 0) {
            return "You win";
        } else if (result < 0) {
            return "You lose";
        } else {
            return "Draw";
        }
    }
}
