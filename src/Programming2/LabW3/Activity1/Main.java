package Programming2.LabW3.Activity1;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        try (Scanner scanner = new Scanner(System.in)) {
            boolean continuePlaying = true;

            showExamples();

            while (continuePlaying) {
                try {
                    playGame(scanner);
                } catch (IllegalArgumentException e) {
                    System.out.println("Error: " + e.getMessage());
                    System.out.println("Do you want to try again? (yes/no):");
                    if (!scanner.nextLine().trim().toLowerCase().equals("yes")) {
                        break;
                    }
                    continue;
                }

                // Ask the user if they want to continue playing
                System.out.println("Do you want to play again? (yes/no):");
                String response = scanner.nextLine().trim().toLowerCase();
                continuePlaying = response.equals("yes");
            }

            System.out.println("Thanks for playing!");
        }
    }

    private static void showExamples() {
        System.out.println("Welcome to the card game. Let's play.");
        System.out.println("Examples of execution:");

        // Example 1
        Card computerCard1 = new Card(Suit.CLUBS, "A");
        Card playerCard1 = new Card(Suit.DIAMONDS, "2");
        System.out.println("Computer: " + computerCard1);
        System.out.println("Player: " + playerCard1);
        System.out.println(playerCard1.determineWinner(computerCard1));

        System.out.println();
        // Example 2
        Card computerCard2 = new Card(Suit.SPADES, "9");
        Card playerCard2 = new Card(Suit.DIAMONDS, "A");
        System.out.println("Computer: " + computerCard2);
        System.out.println("Player: " + playerCard2);
        System.out.println(playerCard2.determineWinner(computerCard2));

        System.out.println();
        // Example 3
        Card computerCard3 = new Card(Suit.CLUBS, "A");
        Card playerCard3 = new Card(Suit.SPADES, "A");
        System.out.println("Computer: " + computerCard3);
        System.out.println("Player: " + playerCard3);
        System.out.println(playerCard3.determineWinner(computerCard3));

        System.out.println();
    }

    public static void playGame(Scanner scanner) {
        // Read computer card
        System.out.println("Enter the computer's card (Example: CA for CLUBS Ace):");
        String computerCardInput = scanner.nextLine();
        Card computerCard = parseCard(computerCardInput);

        // Read player card
        System.out.println("Enter your card (Example: D2 for DIAMONDS 2):");
        String playerCardInput = scanner.nextLine();
        Card playerCard = parseCard(playerCardInput);

        System.out.println("Computer: " + computerCard);
        System.out.println("Player: " + playerCard);

        String result = playerCard.determineWinner(computerCard);
        System.out.println(result);
    }

    private static Card parseCard(String input) {
        if (input.length() < 2) {
            throw new IllegalArgumentException("Invalid card input. Please ensure you include a suit symbol followed by the card value.");
        }
        char suitSymbol = input.charAt(0);
        String value = input.substring(1);

        try {
            Suit suit = Suit.fromSymbol(suitSymbol);
            return new Card(suit, value);
        } catch (IllegalArgumentException e) {
            throw new IllegalArgumentException("Invalid suit symbol. Please use one of the following: C, D, H, S.");
        }
    }
}
