package Programming2.LabW3.Activity3;
import java.util.ArrayList;
import java.util.List;

class Substitute implements Professor {
    private List<Subject> subjects = new ArrayList<>();
    private int currentIndex = 0;

    public void assign(Subject subject) {
        subjects.add(subject);
    }

    @Override
    public void teach() {
        if (currentIndex < subjects.size()) {
            System.out.println("Substitute professor teaching " + subjects.get(currentIndex).name().toLowerCase());
            currentIndex++;
        } else {
            System.out.println("No more subjects assigned.");
        }
    }
}