package Programming2.LabW3.Activity3;

class Tenured implements Professor {
    private Subject subject;

    public Tenured(Subject subject) {
        this.subject = subject;
    }

    @Override
    public void teach() {
        System.out.println("Tenured professor teaching " + subject.name().toLowerCase());
    }
}
