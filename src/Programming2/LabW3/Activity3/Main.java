package Programming2.LabW3.Activity3;

public class Main {
    public static void main(String[] args) {
        Professor tenured = new Tenured(Subject.SCIENCE);
        tenured.teach();

        Substitute substitute = new Substitute();
        substitute.assign(Subject.SCIENCE);
        substitute.assign(Subject.GYM);
        substitute.assign(Subject.MUSIC);

        substitute.teach();
        substitute.teach();
        substitute.teach();
    }
}
