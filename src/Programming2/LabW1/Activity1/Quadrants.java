package Programming2.LabW1.Activity1;

public class Quadrants {
    public static void main(String[] args) {
        if (args.length < 2) {
            return;
        }


        long x, y;
        try {
            x = Long.parseLong(args[0]);
            y = Long.parseLong(args[1]);
        } catch (NumberFormatException e) {
            System.out.println("Invalid input. Please enter valid numbers.");
            return;
        }

        // Check if the coordinates are 0 and 0
        if (x == 0 || y == 0) {
            System.out.println("Origin or ?");
            return;
        }

        //Nested conditions for checking coordinates
        if (x > 0){
            if(y > 0){
                System.out.println("Quadrant 2");
            }
            else{
                System.out.println("Quadrant 4");
            }
        }
        else {
            if(y < 0){
                System.out.println("Quadrant 3");
            }
            else {
                System.out.println("Quadrant 1");
            }
        }
    }
}