package Programming2.LabW1.Activity1;

public class Factorial {
    public static void main(String[] args) {
        int size = Integer.parseInt(args[0]);
        int[] numbers = new int[size];
        int[] factorials = new int[size];

        for (int i = 0; i < size; i++) {
            numbers[i] = Integer.parseInt(args[i + 1]);
        }

        // Calculate Factorial
        for (int i = 0; i < size; i++) {
            int factorial = 1;
            for (int j = 1; j <= numbers[i]; j++) {
                factorial *= j;
            }
            factorials[i] = factorial;
        }

        // Print the last digit of each factorial
        for (int i = 0; i < size; i++) {
            int lastDigit = factorials[i] % 10;
            System.out.println("Last digit of " + numbers[i] + ": " + lastDigit);
        }
    }
}


