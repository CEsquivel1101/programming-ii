package Programming2.LabW1.Activity2.Shapes.Squares;

public class Square extends Rectangle {
    // Attributes
    private double side;

    // Constructor
    public Square(String shapeName, int side) {
        super(shapeName, side, side);
        this.side = side;
    }

    // Methods
    @Override
    public String toString() {
        return String.format("%s, side: %.1f, area: %.2f, perimeter: %.2f",
                getShapeName(), side, calculateArea(), calculatePerimeter());
    }
}
