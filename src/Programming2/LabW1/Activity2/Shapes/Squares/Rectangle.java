package Programming2.LabW1.Activity2.Shapes.Squares;

import Programming2.LabW1.Activity2.Shapes.Shape;

public class Rectangle extends Shape {
    // Attributes
    private double width;
    private double height;

    // Constructor
    public Rectangle(String shapeName, int width, int height) {
        super(shapeName);
        this.width = width;
        this.height = height;
    }

    // Methods
    @Override
    public double calculateArea() {
        return width * height;
    }

    @Override
    public double calculatePerimeter() {
        return 2 * (width + height);
    }

    @Override
    public String toString() {
        return String.format("%s, width: %.1f, height: %.1f, area: %.1f, perimeter: %.1f",
                getShapeName(), width, height, calculateArea(), calculatePerimeter());
    }

}



