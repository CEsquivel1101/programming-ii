package Programming2.LabW1.Activity2.Shapes.Triangles;

public class ScaleneTriangle extends Triangle {
    // Attributes
    private double side1;
    private double side2;
    private double side3;

    // Constructor
    public ScaleneTriangle(String shapeName, double base, double height, double side1, double side2, double side3) {
        super(shapeName, base, height);
        this.side1 = side1;
        this.side2 = side2;
        this.side3 = side3;
    }

    // Methods
    @Override
    public double calculateArea() {
        double s = (side1 + side2 + side3) / 2;
        return Math.sqrt(s * (s - side1) * (s - side2) * (s - side3));
    }

    @Override
    public double calculatePerimeter() {
        return side1 + side2 + side3;
    }

    @Override
    public String toString() {
        return String.format("%s, side1: %.1f, side2: %.1f, side3: %.1f, area: %.3f, perimeter: %.3f",
                getShapeName(), side1, side2, side3, calculateArea(), calculatePerimeter());
    }
}

