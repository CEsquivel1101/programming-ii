package Programming2.LabW1.Activity2.Shapes.Triangles;

import Programming2.LabW1.Activity2.Shapes.Shape;

public abstract class Triangle extends Shape {
    // Attributes
    private double base;
    private double height;

    // Constructor
    public Triangle(String shapeName, double base, double height) {
        super(shapeName);
        this.base = base;
        this.height = height;
    }

    // Getters and Setters
    public double getBase() {
        return base;
    }

    public void setBase(double base) {
        this.base = base;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    // Métodos abstractos para ser implementados por las subclases
    public abstract double calculateArea();
    public abstract double calculatePerimeter();
}
