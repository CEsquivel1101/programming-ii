package Programming2.LabW1.Activity2.Shapes.Triangles;

public class IsoscelesTriangle extends Triangle {
    // Attributes
    private double side;

    // Constructor
    public IsoscelesTriangle(String shapeName, double base, double height, double side) {
        super(shapeName, base, height);
        this.side = side;
    }

    // Methods
    @Override
    public double calculateArea() {
        return (getBase() * getHeight()) / 2;
    }

    @Override
    public double calculatePerimeter() {
        return 2 * side + getBase();
    }

    @Override
    public String toString() {
        return String.format("%s, base: %.1f, height: %.1f, side: %.1f, area: %.3f, perimeter: %.3f",
                getShapeName(), getBase(), getHeight(), side, calculateArea(), calculatePerimeter());
    }
}


