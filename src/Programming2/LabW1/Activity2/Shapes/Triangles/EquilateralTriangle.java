package Programming2.LabW1.Activity2.Shapes.Triangles;

public class EquilateralTriangle extends Triangle {
    // Attributes
    private double side;

    // Constructor
    public EquilateralTriangle(String shapeName, double side) {
        super(shapeName, side, (Math.sqrt(3) / 2) * side);
        this.side = side;
    }

    // Methods
    @Override
    public double calculateArea() {
        return (Math.sqrt(3) / 4) * Math.pow(side, 2);
    }

    @Override
    public double calculatePerimeter() {
        return 3 * side;
    }

    @Override
    public String toString() {
        return String.format("%s, side: %.1f, area: %.3f, perimeter: %.3f",
                getShapeName(), side, calculateArea(), calculatePerimeter());
    }


}

