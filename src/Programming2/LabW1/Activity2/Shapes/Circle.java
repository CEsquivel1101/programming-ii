package Programming2.LabW1.Activity2.Shapes;

public class Circle extends Shape {
    // Attributes
    private double radius;

    // Constructor
    public Circle(String shapeName, double radius) {
        super(shapeName);
        this.radius = radius;
    }

    // Methods
    @Override
    public double calculateArea() {
        return Math.PI * radius * radius;
    }

    @Override
    public double calculatePerimeter() {
        return 2 * Math.PI * radius;
    }

    @Override
    public String toString() {
        return String.format("%s, radius: %.1f, area: %.3f, perimeter: %.4f",
                getShapeName(), radius, calculateArea(), calculatePerimeter());
    }

}

