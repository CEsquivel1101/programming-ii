package Programming2.LabW1.Activity2;

import Programming2.LabW1.Activity2.Shapes.Squares.Rectangle;
import Programming2.LabW1.Activity2.Shapes.Squares.Square;
import Programming2.LabW1.Activity2.Shapes.Triangles.EquilateralTriangle;
import Programming2.LabW1.Activity2.Shapes.Triangles.IsoscelesTriangle;
import Programming2.LabW1.Activity2.Shapes.Triangles.ScaleneTriangle;
import Programming2.LabW1.Activity2.Shapes.Circle;

public class Main {
    public static void main(String[] args) {
        // Create instances of each figure
        Circle circle = new Circle("Circle", 5);
        Circle circle1 = new Circle("Circle 2", 10);
        Rectangle rectangle = new Rectangle("Rectangle", 15, 7);
        EquilateralTriangle triangle = new EquilateralTriangle("EquilateralTriangle", 7);
        ScaleneTriangle scaleneTriangle = new ScaleneTriangle("ScaleneTriangle", 3, 4, 3, 4, 5);
        IsoscelesTriangle isoscelesTriangle = new IsoscelesTriangle("IsoscelesTriangle", 6, 8, 5);
        Square square = new Square("Square", 2);

        // Printing results using toString methods
        System.out.println(circle);
        System.out.println(circle1);
        System.out.println(rectangle);
        System.out.println(triangle);
        System.out.println(square);
        System.out.println(scaleneTriangle);
        System.out.println(isoscelesTriangle);


    }
}

