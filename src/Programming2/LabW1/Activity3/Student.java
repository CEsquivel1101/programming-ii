package Programming2.LabW1.Activity3;

public class Student extends Person {
    private int degree;
    private String[] courses;

    public Student(String name, int age, String gender, int degree, String[] courses) {
        super(name, age, gender);
        this.degree = degree;
        this.courses = courses;
    }

    public int getDegree() {
        return degree;
    }

    public String[] getCourses() {
        return courses;
    }

    public void setDegree(int degree) {
        this.degree = degree;
    }

    public void setCourses(String[] courses) {
        this.courses = courses;
    }

    public void addCourse(String name) {
        String[] newCourses = new String[courses.length + 1];
        System.arraycopy(courses, 0, newCourses, 0, courses.length);
        newCourses[courses.length] = name;
        courses = newCourses;
    }

    @Override
    public String toString() {
        return "Name: " + getName() + ", Grade: " + degree + ", Courses: " + String.join(", ", courses);
    }
}

