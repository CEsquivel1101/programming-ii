package Programming2.LabW1.Activity3;

public class Main {
    public static void main(String[] args) {
        // Create students
        Student student1 = new Student("Liz", 20, "Female", 2, new String[]{"Math", "Physics", "Science"});
        Student student2 = new Student("Luis", 22, "Male", 1, new String[]{"Chemistry", "Biology", "Science"});
        Student student3 = new Student("Raul", 19, "Male", 1, new String[]{"Math", "Science"});

        // Create university and add students
        University university = new University("Jala University");
        university.addStudent(student1);
        university.addStudent(student2);
        university.addStudent(student3);

        // List students with their courses before changes
        System.out.println("List students with their courses before changes:");
        university.listStudentsWithCourses();
        System.out.println();

        // Create new student
        university.newStudent("Gery", 21, "Male", 1, new String[]{"Induction"});

        // Assign a new course to a student
        university.assignNewCourse(3, "Database I");

        // Change student year
        university.newDegree("Luis", 2);

        // Filter students by course
        university.filterStudentByCourse("Science");

        // List students with their courses after changes
        university.listStudentsWithCourses();
    }
}
