package Programming2.LabW1.Activity3;

public class University {
    private String name;
    private Student[] students;
    private int studentCount;

    public University(String name) {
        this.name = name;
        this.students = new Student[10];
        this.studentCount = 0;
    }

    public void addStudent(Student student) {
        if (studentCount == students.length) {
            Student[] newStudents = new Student[students.length * 2];
            System.arraycopy(students, 0, newStudents, 0, students.length);
            students = newStudents;
        }
        students[studentCount++] = student;
    }

    public void newStudent(String name, int age, String gender, int degree, String[] courses) {
        Student newStudent = new Student(name, age, gender, degree, courses);
        addStudent(newStudent);
    }

    public void filterStudentByCourse(String course) {
        StringBuilder studentsInCourse = new StringBuilder("Students of the " + course + " course: ");
        boolean first = true;
        for (int i = 0; i < studentCount; i++) {
            for (String studentCourse : students[i].getCourses()) {
                if (studentCourse.equals(course)) {
                    if (!first) {
                        studentsInCourse.append(", ");
                    }
                    studentsInCourse.append(students[i].getName());
                    first = false;
                    break;
                }
            }
        }
        System.out.println(studentsInCourse.toString());
    }


    public void listStudentsWithCourses() {
        for (int i = 0; i < studentCount; i++) {
            System.out.println(students[i].toString());
        }
    }

    public void assignNewCourse(int studentIndex, String newCourse) {
        if (studentIndex >= 0 && studentIndex < studentCount) {
            students[studentIndex].addCourse(newCourse);
            System.out.println("Student at index " + studentIndex + " was assigned a new course: " + newCourse);
        } else {
            System.out.println("Invalid student index: " + studentIndex);
        }
    }


    public void newDegree(String studentName, int newDegree) {
        for (int i = 0; i < studentCount; i++) {
            if (students[i].getName().equals(studentName)) {
                students[i].setDegree(newDegree);
                break;
            }
        }
    }
}
