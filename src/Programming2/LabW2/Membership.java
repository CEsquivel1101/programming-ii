package Programming2.LabW2;

import java.util.Date;
import java.util.Calendar;

public class Membership {
    private Date membershipDate;
    private Date expirationDate;

    public Membership(int months) {
        this.membershipDate = new Date();
        this.expirationDate = addMonthsToDate(this.membershipDate, months);
    }

    public Date getMembershipDate() {
        return membershipDate;
    }

    public Date getExpirationDate() {
        return expirationDate;
    }

    public void cancelMembership() {
        System.out.println("Membership cancelled on: " + new Date());
        this.expirationDate = new Date();
    }

    public void renewMembership(int months) {
        this.expirationDate = addMonthsToDate(this.expirationDate, months);
        System.out.println("Membership renewed. New expiration date: " + this.expirationDate);
    }

    private Date addMonthsToDate(Date date, int months) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.MONTH, months);
        return cal.getTime();
    }
}