package Programming2.LabW2;

import Programming2.LabW2.person.Librarian;
import Programming2.LabW2.person.Person;
import Programming2.LabW2.person.User;
import java.util.Date;
import java.util.Scanner;

public class Interface {
    private Library library = new Library("City Library", "123 Main St");
    private Scanner scanner = new Scanner(System.in);
    private Person loggedInPerson = null;

    public void start() {
        while (true) {
            showMainMenu();
            int choice = getValidChoice(1, 3);
            switch (choice) {
                case 1:
                    registerPerson();
                    break;
                case 2:
                    loginPerson();
                    break;
                case 3:
                    exit();
                    return;
                default:
                    System.out.println("Invalid choice. Please try again.");
            }
        }
    }

    private void showMainMenu() {
        System.out.println("=== Library Management System ===");
        System.out.println("1. Register Person");
        System.out.println("2. Login Person");
        System.out.println("3. Exit");
        System.out.print("Enter your choice: ");
    }

    private int getValidChoice(int min, int max) {
        while (true) {
            try {
                int choice = Integer.parseInt(scanner.nextLine());
                if (choice >= min && choice <= max) {
                    return choice;
                } else {
                    System.out.println("Invalid choice. Please enter a number between " + min + " and " + max + ".");
                }
            } catch (NumberFormatException e) {
                System.out.println("Invalid input. Please enter a number.");
            }
        }
    }

    private void registerPerson() {
        String name;
        while (true) {
            System.out.print("Enter name: ");
            name = scanner.nextLine();
            if (!name.matches("[a-zA-Z ]+")) {
                System.out.println("Invalid name.");
            } else {
                break;
            }
        }

        String email;
        while (true) {
            System.out.print("Enter email: ");
            email = scanner.nextLine();
            if (!email.matches("^[\\w-.]+@([\\w-]+\\.)+[\\w-]{2,4}$") || !email.endsWith(".com")) {
                System.out.println("Invalid email.");
            } else {
                break;
            }
        }

        String id;
        while (true) {
            System.out.print("Enter ID (6 digits): ");
            id = scanner.nextLine();
            if (!id.matches("\\d{6}")) {
                System.out.println("Invalid ID. ID should be exactly 6 digits.");
            } else if (library.getPersonById(id) != null) {
                System.out.println("Please enter a unique ID.");
            } else {
                break;
            }
        }

        String password;
        while (true) {
            System.out.print("Enter password (8 characters): ");
            password = scanner.nextLine();
            if (password.length() != 8) {
                System.out.println("Invalid password. Password should be exactly 8 characters.");
            } else {
                break;
            }
        }

        System.out.print("Are you a Librarian (L) or a User (U)? ");
        char personType = scanner.nextLine().toLowerCase().charAt(0);

        if (personType == 'l') {
            Librarian librarian = new Librarian(name, email, id, password);
            library.registerPerson(librarian);
        } else if (personType == 'u') {
            User user = new User(name, email, id, password);
            library.registerPerson(user);
        } else {
            System.out.println("Invalid person type.");
        }
    }

    public void loginPerson() {
        System.out.print("Enter ID: ");
        String id = scanner.nextLine();
        System.out.print("Enter password: ");
        String password = scanner.nextLine();

        Person person = library.getPersonById(id);
        if (person != null && person instanceof User && ((User) person).getPassword().equals(password)) {
            if (((User) person).getMembership().getExpirationDate().before(new Date())) {
                System.out.println("Membership expired. Please renew your membership with a librarian.");
                return;
            }
            loggedInPerson = person;
            System.out.println("Login successful! Welcome " + person.getName());
            showUserMenu((User) person);
        } else if (person != null && person instanceof Librarian && ((Librarian) person).getPassword().equals(password)) {
            loggedInPerson = person;
            System.out.println("Login successful! Welcome " + person.getName());
            showLibrarianMenu((Librarian) person);
        } else {
            System.out.println("Invalid ID or password.");
        }
    }

    private void showLibrarianMenu(Librarian librarian) {
        while (true) {
            System.out.println("=== Librarian Menu ===");
            System.out.println("1. Add Book");
            System.out.println("2. Remove Book");
            System.out.println("3. Set Book Availability");
            System.out.println("4. Cancel User Membership");
            System.out.println("5. Renew User Membership");
            System.out.println("6. Logout");
            System.out.print("Enter your choice: ");
            int choice = getValidChoice(1, 6);
            switch (choice) {
                case 1:
                    addBook(librarian);
                    break;
                case 2:
                    removeBook(librarian);
                    break;
                case 3:
                    setBookAvailability(librarian);
                    break;
                case 4:
                    cancelUserMembership(librarian);
                    break;
                case 5:
                    renewUserMembership(librarian);
                    break;
                case 6:
                    loggedInPerson = null;
                    return;
                default:
                    System.out.println("Invalid choice. Please try again.");
            }
        }
    }

    private void addBook(Librarian librarian) {
        System.out.print("Enter book title: ");
        String title = scanner.nextLine();
        System.out.print("Enter book author: ");
        String author = scanner.nextLine();
        librarian.addBook(library, title, author);
    }

    private void removeBook(Librarian librarian) {
        System.out.print("Enter book title: ");
        String title = scanner.nextLine();
        librarian.removeBook(library, title);
    }

    private void setBookAvailability(Librarian librarian) {
        System.out.print("Enter book title: ");
        String title = scanner.nextLine();
        System.out.print("Set availability (true/false): ");
        boolean available = getValidBoolean();
        librarian.setBookAvailability(library, title, available);
    }

    private void cancelUserMembership(Librarian librarian) {
        System.out.print("Enter user ID: ");
        String userId = scanner.nextLine();
        librarian.cancelMembership(library, userId);
    }

    private void renewUserMembership(Librarian librarian) {
        System.out.print("Enter user ID: ");
        String userId = scanner.nextLine();
        System.out.print("Enter renewal period in months (6 or 12): ");
        int months = getValidChoice(6, 12);
        librarian.renewMembership(library, userId, months);
    }

    private boolean getValidBoolean() {
        while (true) {
            String input = scanner.nextLine().toLowerCase();
            if (input.equals("true") || input.equals("false")) {
                return Boolean.parseBoolean(input);
            } else {
                System.out.println("Invalid input. Please enter 'true' or 'false'.");
            }
        }
    }

    private void showUserMenu(User user) {
        while (true) {
            System.out.println("=== User Menu ===");
            System.out.println("1. Borrow Book");
            System.out.println("2. Return Book");
            System.out.println("3. Write Review");
            System.out.println("4. Check Membership Status");
            System.out.println("5. Logout");
            System.out.print("Enter your choice: ");
            int choice = getValidChoice(1, 5);
            switch (choice) {
                case 1:
                    borrowBook(user);
                    break;
                case 2:
                    returnBook(user);
                    break;
                case 3:
                    writeReview(user);
                    break;
                case 4:
                    user.checkMembershipStatus();
                    break;
                case 5:
                    loggedInPerson = null;
                    return;
                default:
                    System.out.println("Invalid choice. Please try again.");
            }
        }
    }

    private void borrowBook(User user) {
        System.out.print("Enter book title: ");
        String title = scanner.nextLine();
        Book book = library.getBookByTitle(title);
        if (book == null || !book.isAvailable()) {
            System.out.println("Book not found.");
            return;
        }
        System.out.print("Enter return date (yyyy-mm-dd): ");
        Date returnDate = getValidDate();
        user.borrowBook(library, title, returnDate);
    }

    private void returnBook(User user) {
        System.out.print("Enter book title: ");
        String title = scanner.nextLine();
        user.returnBook(library, title);
    }

    private void writeReview(User user) {
        System.out.print("Enter book title: ");
        String title = scanner.nextLine();
        System.out.print("Enter review content: ");
        String content = scanner.nextLine();
        System.out.print("Enter rating (1-5): ");
        int rating = getValidChoice(1, 5);

        Review review = new Review(content, rating);
        user.writeReview(library, title, review);
    }

    private Date getValidDate() {
        while (true) {
            try {
                String dateStr = scanner.nextLine();
                return java.sql.Date.valueOf(dateStr);
            } catch (IllegalArgumentException e) {
                System.out.println("Invalid date format. Please enter in yyyy-mm-dd format.");
            }
        }
    }

    private void exit() {
        System.out.println("Exiting the system...");
    }
}