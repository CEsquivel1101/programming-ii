package Programming2.LabW2;

import Programming2.LabW2.person.Librarian;
import Programming2.LabW2.person.Person;
import Programming2.LabW2.person.User;

import java.util.ArrayList;
import java.util.List;

public class Library {
    private String name;
    private String address;
    private List<Book> books = new ArrayList<>();
    private List<Person> people = new ArrayList<>();

    public Library(String name, String address) {
        this.name = name;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void addBook(Book book) {
        books.add(book);
        System.out.println("Book added: " + book.getTitle());
    }

    public void removeBook(Book book) {
        books.remove(book);
        System.out.println("Book removed: " + book.getTitle());
    }

    public void registerPerson(Person person) {
        people.add(person);
        System.out.println(person.getName() + " registered.");
    }

    public Person getPersonById(String id) {
        for (Person person : people) {
            if (person instanceof User && ((User) person).getId().equals(id)) {
                return person;
            } else if (person instanceof Librarian && ((Librarian) person).getId().equals(id)) {
                return person;
            }
        }
        return null;
    }

    public Book getBookByTitle(String title) {
        for (Book book : books) {
            if (book.getTitle().equalsIgnoreCase(title)) {
                return book;
            }
        }
        return null;
    }

    public List<Book> getBooks() {
        return books;
    }

    public List<Person> getPeople() {
        return people;
    }
}