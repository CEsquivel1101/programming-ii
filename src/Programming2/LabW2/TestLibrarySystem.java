package Programming2.LabW2;

import Programming2.LabW2.person.Librarian;
import Programming2.LabW2.person.User;
import java.util.Date;

public class TestLibrarySystem {
    public static void main(String[] args) {
// Create a library instance
        Library library = new Library("City Library", "123 Main St");

// Register a user
        User user = new User("Alice", "alice@example.com", "123456", "password123");
        library.registerPerson(user);

// Register a librarian
        Librarian librarian = new Librarian("Bob", "bob@example.com", "654321", "password456");
        library.registerPerson(librarian);

// Display the user's membership status
        user.checkMembershipStatus();

// Librarian adds a book to the library
        librarian.addBook(library, "1984", "George Orwell");

// Librarian adds another book to the library
        librarian.addBook(library, "To Kill a Mockingbird", "Harper Lee");

// User tries to borrow an available book
        System.out.println("Borrowing '1984'...");
        Date returnDate = new Date(System.currentTimeMillis() + (7L * 24 * 60 * 60 * 1000)); // 7 days from today
        user.borrowBook(library, "1984", returnDate);

// User tries to borrow an unavailable book
        System.out.println("Borrowing '1984' again...");
        user.borrowBook(library, "1984", returnDate);

// User tries to borrow a book that does not exist
        System.out.println("Borrowing 'Nonexistent Book'...");
        user.borrowBook(library, "Nonexistent Book", returnDate);

// Librarian changes the availability of a book
        System.out.println("Setting availability of 'To Kill a Mockingbird' to false...");
        librarian.setBookAvailability(library, "To Kill a Mockingbird", false);

// User attempts to borrow an unavailable book
        System.out.println("Borrowing 'To Kill a Mockingbird'...");
        user.borrowBook(library, "To Kill a Mockingbird", returnDate);

// User returns a book
        System.out.println("Returning '1984'...");
        user.returnBook(library, "1984");

// User attempts to return a book that was not borrowed
        System.out.println("Returning 'To Kill a Mockingbird'...");
        user.returnBook(library, "To Kill a Mockingbird");

// Librarian cancels user's membership
        System.out.println("Cancelling membership of user 'Alice'...");
        librarian.cancelMembership(library, "123456");

// User tries to log in with cancelled membership
        Interface libraryInterface = new Interface();
        libraryInterface.start();
        System.out.println("Trying to log in with cancelled membership...");
        libraryInterface.loginPerson();

// Librarian renews user's membership
        System.out.println("Renewing membership of user 'Alice' for 6 months...");
        librarian.renewMembership(library, "123456", 6);

// User tries to log in after renewing membership
        System.out.println("Trying to log in with renewed membership...");
        libraryInterface.loginPerson();
    }
}