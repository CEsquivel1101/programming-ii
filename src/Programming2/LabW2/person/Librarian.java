package Programming2.LabW2.person;

import Programming2.LabW2.Book;
import Programming2.LabW2.Library;

public class Librarian extends Person {
    private String id;
    private String password;

    public Librarian(String name, String email, String id, String password) {
        super(name, email);
        this.id = id;
        this.password = password;
    }

    public String getId() {
        return id;
    }

    public String getPassword() {
        return password;
    }

    public void manageBooks() {
        System.out.println("Managing books...");
    }

    public void addBook(Library library, String title, String author) {
        Book book = new Book(title, author);
        library.addBook(book);
    }

    public void removeBook(Library library, String title) {
        Book book = library.getBookByTitle(title);
        if (book != null) {
            library.removeBook(book);
        } else {
            System.out.println("Book not found.");
        }
    }

    public void setBookAvailability(Library library, String title, boolean available) {
        Book book = library.getBookByTitle(title);
        if (book != null) {
            book.setAvailable(available);
            System.out.println("Book availability set to: " + available);
        } else {
            System.out.println("Book not found.");
        }
    }

    public void cancelMembership(Library library, String userId) {
        User user = (User) library.getPersonById(userId);
        if (user != null) {
            user.getMembership().cancelMembership();
        } else {
            System.out.println("User not found.");
        }
    }

    public void renewMembership(Library library, String userId, int months) {
        User user = (User) library.getPersonById(userId);
        if (user != null) {
            user.getMembership().renewMembership(months);
        } else {
            System.out.println("User not found.");
        }
    }

    @Override
    public void performTask() {
        System.out.println("Performing librarian tasks...");
    }
}