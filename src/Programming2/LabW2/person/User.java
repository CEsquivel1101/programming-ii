package Programming2.LabW2.person;

import Programming2.LabW2.Book;
import Programming2.LabW2.Library;
import Programming2.LabW2.Review;
import Programming2.LabW2.Membership;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

public class User extends Person {
    private String id;
    private String password;
    private List<Book> borrowedBooks = new ArrayList<>();
    private Membership membership;

    public User(String name, String email, String id, String password) {
        super(name, email);
        this.id = id;
        this.password = password;
        this.membership = new Membership(1); // 1 month trial
    }

    public String getId() {
        return id;
    }

    public String getPassword() {
        return password;
    }

    public Membership getMembership() {
        return membership;
    }

    public void borrowBook(Library library, String bookTitle, Date returnDate) {
        Book book = library.getBookByTitle(bookTitle);
        if (book == null || !book.isAvailable()) {
            System.out.println("Book not found.");
            return;
        }

        borrowedBooks.add(book);
        book.setAvailable(false);
        System.out.println(getName() + " has borrowed the book: " + book.getTitle() + " with return date: " + returnDate);
    }

    public void returnBook(Library library, String bookTitle) {
        Book book = library.getBookByTitle(bookTitle);
        if (book == null) {
            System.out.println("Book not found.");
            return;
        }

        if (borrowedBooks.remove(book)) {
            book.setAvailable(true);
            System.out.println(getName() + " has returned the book: " + book.getTitle());
        } else {
            System.out.println("This book was not borrowed by you.");
        }
    }

    public void writeReview(Library library, String bookTitle, Review review) {
        Book book = library.getBookByTitle(bookTitle);
        if (book == null) {
            System.out.println("Book not found.");
            return;
        }
        book.addReview(review);
        System.out.println("Review added for book: " + book.getTitle());
    }

    @Override
    public void performTask() {
        System.out.println(getName() + " is performing user tasks.");
    }

    public void checkMembershipStatus() {
        Date now = new Date();
        long diff = membership.getExpirationDate().getTime() - now.getTime();
        long daysRemaining = diff / (1000 * 60 * 60 * 24);
        System.out.println("You have " + daysRemaining + " days remaining in your membership.");
    }
}